#ifndef TOPDILEPTONEVENTSAVER_H_
#define TOPDILEPTONEVENTSAVER_H_

#include "TopAnalysis/EventSaverFlatNtuple.h"
//#include "TopDileptonAnalysis/solution.h"
#include "TRandom3.h"
#include "TH2.h"
#include "TopEventSelectionTools/PlotManager.h"
//#include "TopDileptonAnalysis/MakeHists.h"

//#include "TopConfiguration/TopConfig.h"

//#include "TopDileptonAnalysis/WeightSet.h"

//namespace top{

class TopDileptonEventSaver : public top::EventSaverFlatNtuple {
    //class TopDileptonEventSaver : public top::EventSaverFlatNtuple, public top::EventSaverBase , public asg::AsgTool  {

public:

  ///-- Default - so root can load based on a name --///
  TopDileptonEventSaver();

  ///-- Default - so we can clean up --///
  //~TopDileptonEventSaver();//=0;
  
  ///-- Exposition of base class initializers (to avoid [-Woverloaded-virtual] hidden warnings --///
  using top::EventSaverFlatNtuple::initialize;
  
  ///-- Run once at the start of the job --///
  void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches);

  ///-- Run for every event (in every systematic) that needs saving --///
  void initializeRecoLevelBranches();
  void saveEvent(const top::Event& event);

  ///-- For particle level object --///
  void setupParticleLevelTreeManager(const top::ParticleLevelEvent& plEvent);
  void saveParticleLevelEvent(const top::ParticleLevelEvent& plEvent);
  void initializeParticleLevelBranches();

  ///-- For parton level objects --///
  void initializePartonLevelEvent();
  void setupPartonLevelEvent();
  void saveTruthEvent();
  
  ///-- Functions for event selection flags --///
  bool passEMu();
  bool passEMu_pretag();
  bool passEMu_control();
  bool passParticleEMu();

  //-- binning function --//
  int findBin(float var, float *bins, int size);

  //-- spin correlation functions  --//
  float calculateCosCos(TLorentzVector top, TLorentzVector tbar, TLorentzVector lep_pos, TLorentzVector lep_neg);

  float cos_theta_helicity(   TLorentzVector t, TLorentzVector parent_t, TLorentzVector ttbar, TLorentzVector lep, float sign);
  float cos_theta_raxis(      TLorentzVector t, TLorentzVector parent_t, TLorentzVector ttbar, TLorentzVector lep, float sign);
  float cos_theta_transverse( TLorentzVector t, TLorentzVector parent_t, TLorentzVector ttbar, TLorentzVector lep, float sign);
  float opening_angle(        TLorentzVector ttbar, TLorentzVector lep_p, TLorentzVector lep_n);
  float phi_star(             TLorentzVector matter, TLorentzVector anti_matter);
  float calculateDphi_ttbar(TLorentzVector t, TLorentzVector tbar);
  float calculatePout(TLorentzVector t, TLorentzVector tbar);


 private:

  ///-- Spin Correlation and Polarisaton variables --///
  float m_reco_cos_helicity_p;
  float m_reco_cos_helicity_m;
  float m_reco_cos_raxis_p;
  float m_reco_cos_raxis_m;
  float m_reco_cos_transverse_p;
  float m_reco_cos_transverse_m;
  float m_reco_opening_angle;
  float m_reco_phi_star_t_tbar;

  float m_particle_cos_helicity_p;
  float m_particle_cos_helicity_m;
  float m_particle_cos_raxis_p;
  float m_particle_cos_raxis_m;
  float m_particle_cos_transverse_p;
  float m_particle_cos_transverse_m;
  float m_particle_opening_angle;
  float m_particle_phi_star_t_tbar;

  float m_parton_cos_helicity_p;
  float m_parton_cos_helicity_m;
  float m_parton_cos_raxis_p;
  float m_parton_cos_raxis_m;
  float m_parton_cos_transverse_p;
  float m_parton_cos_transverse_m;
  float m_parton_opening_angle;
  float m_parton_phi_star_t_tbar;

  ///-- Steering --///
  bool is_4bjets;
  bool do_different_setting;
  bool is_MC;
  std::shared_ptr<top::TopConfig> m_config;
  std::shared_ptr<top::TreeManager> m_NominalTreeManager;
  std::shared_ptr<top::TreeManager> m_truthTreeManager;

  ///-- Extra variable to write out --///
  int m_el_n;
  int m_mu_n;
  int m_lep_n;
  int jet_n;
  int m_jet_n;
  int m_bjet_n;
  int m_bjet_n_85;
  int m_bjet_n_77;
  int m_bjet_n_70;
  int m_bjet_n_60;

  bool m_pass_ee;
  bool m_pass_mumu;
  bool m_pass_emu;

  bool m_pass_ee_pretag;
  bool m_pass_mumu_pretag;
  bool m_pass_emu_pretag;

  bool m_pass_control_emu;
  bool m_pass_control_ee;
  bool m_pass_control_mumu;



  float m_b_pt;
  float m_b_eta;
  float m_b_phi;
  float m_b_e;
  float m_b_m;
  float m_b_y;

  float m_bbar_pt;
  float m_bbar_eta;
  float m_bbar_phi;
  float m_bbar_e;
  float m_bbar_m;
  float m_bbar_y;

  float m_top_pt;
  float m_top_eta;
  float m_top_phi;
  float m_top_e;
  float m_top_m;
  float m_top_y;
  float m_top_ratio;

  float m_tbar_pt;
  float m_tbar_eta;
  float m_tbar_phi;
  float m_tbar_e;
  float m_tbar_m;
  float m_tbar_y;
  float m_tbar_ratio;

  float m_av_top_pt;
  float m_av_top_eta;
  float m_av_top_phi;
  float m_av_top_e;
  float m_av_top_m;
  float m_av_top_y;

  float m_t1_pt;
  float m_t1_eta;
  float m_t1_phi;
  float m_t1_e;
  float m_t1_m;
  float m_t1_y;

  float m_t2_pt;
  float m_t2_eta;
  float m_t2_phi;
  float m_t2_e;
  float m_t2_m;
  float m_t2_y;

  float m_ttbar_pt;
  float m_ttbar_eta;
  float m_ttbar_phi;
  float m_ttbar_e;
  float m_ttbar_m;
  float m_ttbar_y;

  float m_b_prime_pt;
  float m_b_prime_px;
  float m_b_prime_py;
  float m_b_prime_pz;
  float m_b_prime_eta;
  float m_b_prime_phi;
  float m_b_prime_e;
  float m_b_prime_m;
  float m_b_prime_y;

  float m_bbar_prime_pt;
  float m_bbar_prime_px;
  float m_bbar_prime_py;
  float m_bbar_prime_pz;
  float m_bbar_prime_eta;
  float m_bbar_prime_phi;
  float m_bbar_prime_e;
  float m_bbar_prime_m;
  float m_bbar_prime_y;



  float m_bbbar_prime_pt;
  float m_bbbar_prime_eta;
  float m_bbbar_prime_phi;
  //float m_bbbar_e;
  float m_bbbar_prime_m;
  //float m_bbbar_y;
  //float m_bbbar_coscos;

  float m_bbbar_prime_delta_pt;
  float m_bbbar_prime_delta_eta;
  float m_bbbar_prime_delta_phi;
  //float m_bbbar_e;
  float m_bbbar_prime_dR;
  float m_bbbar_prime_dy;
  float m_bbbar_prime_dabsy;

  int m_b_index;
  int m_bbar_index;
  int m_b_prime_index;
  int m_bbar_prime_index;

  float m_nu_pt;
  float m_nu_eta;
  float m_nu_phi;
  float m_nu_e;
  float m_nu_m;
  float m_nu_y;

  float m_nubar_pt;
  float m_nubar_eta;
  float m_nubar_phi;
  float m_nubar_e;
  float m_nubar_m;
  float m_nubar_y;

  float m_Wp_pt;
  float m_Wp_eta;
  float m_Wp_phi;
  float m_Wp_e;
  float m_Wp_m;
  float m_Wp_y;

  float m_Wm_pt;
  float m_Wm_eta;
  float m_Wm_phi;
  float m_Wm_e;
  float m_Wm_m;
  float m_Wm_y;

  int   m_particle_eventNumber;
  bool  m_particle_tau_event;

  TLorentzVector m_parton_top;
  TLorentzVector m_parton_tbar;
  TLorentzVector m_parton_b;
  TLorentzVector m_parton_bbar;
  TLorentzVector m_parton_l;
  TLorentzVector m_parton_lbar;
  TLorentzVector m_parton_nu;
  TLorentzVector m_parton_nubar;
  TLorentzVector m_parton_Wp;
  TLorentzVector m_parton_Wm;

  float m_parton_dilep_delta_phi;
  float m_parton_dilep_delta_eta;

  float m_parton_top_pt;
  float m_parton_top_eta;
  float m_parton_top_phi;
  float m_parton_top_e;
  float m_parton_top_m;
  float m_parton_top_y;

  float m_parton_tbar_pt;
  float m_parton_tbar_eta;
  float m_parton_tbar_phi;
  float m_parton_tbar_e;
  float m_parton_tbar_m;
  float m_parton_tbar_y;
  
  float m_parton_av_top_pt;
  float m_parton_av_top_eta;
  float m_parton_av_top_phi;
  float m_parton_av_top_e;
  float m_parton_av_top_m;
  float m_parton_av_top_y;

  float m_parton_t1_pt;
  float m_parton_t1_eta;
  float m_parton_t1_phi;
  float m_parton_t1_e;
  float m_parton_t1_m;
  float m_parton_t1_y;

  float m_parton_t2_pt;
  float m_parton_t2_eta;
  float m_parton_t2_phi;
  float m_parton_t2_e;
  float m_parton_t2_m;
  float m_parton_t2_y;

  float m_parton_ttbar_pt;
  float m_parton_ttbar_eta;
  float m_parton_ttbar_phi;
  float m_parton_ttbar_e;
  float m_parton_ttbar_m;
  float m_parton_ttbar_y;

  float m_parton_nu_pt;
  float m_parton_nu_eta;
  float m_parton_nu_phi;
  float m_parton_nu_e;
  int m_parton_nu_pdgId;

  float m_parton_nubar_pt;
  float m_parton_nubar_eta;
  float m_parton_nubar_phi;
  float m_parton_nubar_e;
  int m_parton_nubar_pdgId;

  float m_parton_b_pt;
  float m_parton_b_eta;
  float m_parton_b_phi;
  float m_parton_b_e;

  float m_parton_bbar_pt;
  float m_parton_bbar_eta;
  float m_parton_bbar_phi;
  float m_parton_bbar_e;

  float m_parton_b_prime_pt;
  float m_parton_b_prime_eta;
  float m_parton_b_prime_phi;
  float m_parton_b_prime_e;

  float m_parton_bbar_prime_pt;
  float m_parton_bbar_prime_eta;
  float m_parton_bbar_prime_phi;
  float m_parton_bbar_prime_e;

  float m_parton_bbbar_prime_pt;
  float m_parton_bbbar_prime_eta;
  float m_parton_bbbar_prime_phi;
  float m_parton_bbbar_prime_e;

  float m_parton_l_pt;
  float m_parton_l_eta;
  float m_parton_l_phi;
  float m_parton_l_e;
  int m_parton_l_pdgId;

  float m_parton_lbar_pt;
  float m_parton_lbar_eta;
  float m_parton_lbar_phi;
  float m_parton_lbar_e;
  int m_parton_lbar_pdgId;

  float m_parton_Wp_pt;
  float m_parton_Wp_eta;
  float m_parton_Wp_phi;
  float m_parton_Wp_e;

  float m_parton_Wm_pt;
  float m_parton_Wm_eta;
  float m_parton_Wm_phi;
  float m_parton_Wm_e;

  float m_parton_weight_mc;
  float m_parton_eventNumber;
  float m_parton_runNumber;

  int m_jet_from_top_index;
  int m_jet_from_tbar_index;

  float m_non_top_mass;

  //----------
  //Angular variables
  //----------
  float m_ttbar_dphi;  
  float m_ttbar_pout;  
  float m_particle_ttbar_dphi;  
  float m_particle_ttbar_pout;  
  float m_parton_ttbar_dphi;  
  float m_parton_ttbar_pout;  

  std::vector<float> m_bjet_pt;
  std::vector<float> m_bjet_eta;
  std::vector<float> m_bjet_phi;
  std::vector<float> m_bjet_e;
  std::vector<float> m_bjet_jvt;
  std::vector<float> m_bjet_jvc;

  std::vector<float> m_jet_pt;
  std::vector<float> m_jet_eta;
  std::vector<float> m_jet_phi;
  std::vector<float> m_jet_e;
  std::vector<float> m_jet_jvf;
  std::vector<float> m_jet_jvt;
  std::vector<float> m_jet_jvc;
  std::vector<float> m_jet_truth;
  std::vector<float> m_jet_mv1;
  std::vector<float> m_jet_mv2_c00;
  std::vector<float> m_jet_mv2_c10;
  std::vector<float> m_jet_mv2_c20;

  std::vector<float> m_lep_pt;
  std::vector<float> m_lep_eta;
  std::vector<float> m_lep_phi;
  std::vector<float> m_lep_e;
  std::vector<float> m_lep_type;
  std::vector<float> m_lep_charge;

  std::vector<float> m_lep_truth_pt;
  std::vector<float> m_lep_truth_eta;
  std::vector<float> m_lep_truth_phi;
  std::vector<float> m_lep_truth_e;
  std::vector<float> m_lep_truth_m;
  std::vector<float> m_lep_truth_status;
  std::vector<int>   m_lep_truth_type;

  std::vector<float> m_lep_topoEtCone20;
  std::vector<float> m_lep_topoEtCone30;
  std::vector<float> m_lep_topoEtCone40;
  std::vector<float> m_lep_ptVarCone20;
  std::vector<float> m_lep_ptVarCone30;
  std::vector<float> m_lep_ptVarCone40;

  std::vector<int>   m_lep_iso_gradient;
  std::vector<int>   m_lep_iso_gradient_loose;


  std::vector<char>   m_lep_id_medium;
  std::vector<char>   m_lep_id_tight;

  std::vector<float> m_lep_dR_jet;
  std::vector<float> m_lep_dR_bjet;
  std::vector<float> m_lep_dR_lep;

  //std::vector<int>  m_lep_match_mu24;
  //std::vector<int>  m_lep_match_mu50;
  //std::vector<int>  m_lep_match_e26;
  //std::vector<int>  m_lep_match_e60;

  std::vector<int>  m_lep_match_e24_lhmedium_L1EM18VH;
  std::vector<int>  m_lep_match_e24_lhmedium_L1EM20VH;
  std::vector<int>  m_lep_match_e60_lhmedium;
  std::vector<int>  m_lep_match_e120_lhloose;
  std::vector<int>  m_lep_match_2e12_lhloose_L12EM10VH;
  std::vector<int>  m_lep_match_2e17_loose;
  std::vector<int>  m_lep_match_mu20_iloose_L1MU15;
  std::vector<int>  m_lep_match_mu24_iloose_L1MU15;
  std::vector<int>  m_lep_match_mu24_imedium;
  std::vector<int>  m_lep_match_mu26_imedium;
  std::vector<int>  m_lep_match_mu50;
  std::vector<int>  m_lep_match_mu18_mu8noL1;
  std::vector<int>  m_lep_match_mu22_mu8noL1;
  std::vector<int>  m_lep_match_mu24_mu8noL1;
  std::vector<int>  m_lep_match_2mu10;
  std::vector<int>  m_lep_match_2mu14;
  std::vector<int>  m_lep_match_e17_loose_mu14;
  std::vector<int>  m_lep_match_e24_medium_L1EM20VHI_mu8noL1;
  std::vector<int>  m_lep_match_e7_medium_mu24;

  std::vector<float> m_lep_trig_SF;
  std::vector<float> m_lep_trig_SF_UP_STAT; // ee and mumu
  std::vector<float> m_lep_trig_SF_DOWN_STAT;
  std::vector<float> m_lep_trig_SF_UP_SYST; // mumu only
  std::vector<float> m_lep_trig_SF_DOWN_SYST;

  //--- Event Level Quantities ---//
  float m_dilep_m;
  float m_dilep_pt;
  float m_dilep_delta_phi;
  float m_dilep_delta_eta;
  std::vector<float> m_mlb;
  float m_met_et;
  float m_met_phi;
  float m_met_ex;
  float m_met_ey;
  float m_met_sumet;
  float m_chi;
  float m_parton_chi;
  float m_particle_chi;

  //  int   m_trig_mu24;
  //  int   m_trig_mu50;
  //  int   m_trig_e26;
  //  int   m_trig_e60;
  int   m_trig_e24_lhmedium_L1EM18VH;
  int   m_trig_e24_lhmedium_L1EM20VH;
  int   m_trig_e60_lhmedium;
  int   m_trig_e120_lhloose;
  int   m_trig_2e12_lhloose_L12EM10VH;
  int   m_trig_2e17_loose;
  int   m_trig_mu20_iloose_L1MU15;
  int   m_trig_mu24_iloose_L1MU15;
  int   m_trig_mu24_imedium;
  int   m_trig_mu26_imedium;
  int   m_trig_mu50;
  int   m_trig_mu18_mu8noL1;
  int   m_trig_mu22_mu8noL1;
  int   m_trig_mu24_mu8noL1;
  int   m_trig_2mu10;
  int   m_trig_2mu14;
  int   m_trig_e17_loose_mu14;
  int   m_trig_e24_medium_L1EM20VHI_mu8noL1;
  int   m_trig_e7_medium_mu24;

  float m_trig_SF;
  float m_trig_SF_UP_STAT;
  float m_trig_SF_DOWN_STAT;
  float m_trig_SF_UP_SYST;
  float m_trig_SF_DOWN_SYST;

  int   m_lumi_block;
  bool  m_fakeEvent;

  TLorentzVector m_particle_neutrino_1;
  TLorentzVector m_particle_neutrino_2;

  int m_particle_lep_n;
  int m_particle_jet_n;
  int m_particle_bjet_n;
  int m_particle_nu_n;

  bool m_particle_pass_ee;
  bool m_particle_pass_mumu;
  bool m_particle_pass_emu;

  float m_particle_top_pt;
  float m_particle_top_eta;
  float m_particle_top_phi;
  float m_particle_top_e;
  float m_particle_top_m;
  float m_particle_top_y;
  float m_particle_top_ratio;

  float m_particle_tbar_pt;
  float m_particle_tbar_eta;
  float m_particle_tbar_phi;
  float m_particle_tbar_e;
  float m_particle_tbar_m;
  float m_particle_tbar_y;
  float m_particle_tbar_ratio;

  float m_particle_av_top_pt;
  float m_particle_av_top_eta;
  float m_particle_av_top_phi;
  float m_particle_av_top_e;
  float m_particle_av_top_m;
  float m_particle_av_top_y;

  float m_particle_t1_pt;
  float m_particle_t1_eta;
  float m_particle_t1_phi;
  float m_particle_t1_e;
  float m_particle_t1_m;
  float m_particle_t1_y;

  float m_particle_t2_pt;
  float m_particle_t2_eta;
  float m_particle_t2_phi;
  float m_particle_t2_e;
  float m_particle_t2_m;
  float m_particle_t2_y;
 
  float m_particle_ttbar_pt;
  float m_particle_ttbar_eta;
  float m_particle_ttbar_phi;
  float m_particle_ttbar_e;
  float m_particle_ttbar_m;
  float m_particle_ttbar_y;
  
  float m_particle_nu_pt;
  float m_particle_nu_eta;
  float m_particle_nu_phi;
  float m_particle_nu_e;
  float m_particle_nu_m;
  float m_particle_nu_y;

  float m_particle_nubar_pt;
  float m_particle_nubar_eta;
  float m_particle_nubar_phi;
  float m_particle_nubar_e;
  float m_particle_nubar_m;
  float m_particle_nubar_y;

  float m_particle_Wp_pt;
  float m_particle_Wp_eta;
  float m_particle_Wp_phi;
  float m_particle_Wp_e;
  float m_particle_Wp_m;
  float m_particle_Wp_y;

  float m_particle_Wm_pt;
  float m_particle_Wm_eta;
  float m_particle_Wm_phi;
  float m_particle_Wm_e;
  float m_particle_Wm_m;
  float m_particle_Wm_y;

  std::vector<float> m_particle_bjet_pt;
  std::vector<float> m_particle_bjet_eta;
  std::vector<float> m_particle_bjet_phi;
  std::vector<float> m_particle_bjet_e;
  std::vector<float> m_particle_bjet_ghosts;
  int m_particle_b_index;
  int m_particle_bbar_index;

  std::vector<float> m_particle_jet_pt;
  std::vector<float> m_particle_jet_eta;
  std::vector<float> m_particle_jet_phi;
  std::vector<float> m_particle_jet_e;
  std::vector<float> m_particle_jet_ghosts;

  std::vector<float> m_particle_lep_pt;
  std::vector<float> m_particle_lep_eta;
  std::vector<float> m_particle_lep_phi;
  std::vector<float> m_particle_lep_e;
  std::vector<float> m_particle_lep_type;
  std::vector<float> m_particle_lep_charge;

  float m_particle_dilep_m;
  float m_particle_dilep_pt;
  float m_particle_dilep_delta_phi;
  float m_particle_dilep_delta_eta;

  float m_particle_met_et;
  float m_particle_met_phi;
  float m_particle_met_ex;
  float m_particle_met_ey;

  int m_top_pt_top_y;
  int m_top_pt_ttbar_pt;
  int m_top_pt_ttbar_y;
  int m_top_pt_ttbar_m;
  int m_top_y_ttbar_pt;
  int m_top_y_ttbar_y;
  int m_top_y_ttbar_m;
  int m_ttbar_pt_ttbar_y;
  int m_ttbar_pt_ttbar_m;
  int m_ttbar_y_ttbar_m;

  int m_particle_top_pt_top_y;
  int m_particle_top_pt_ttbar_pt;
  int m_particle_top_pt_ttbar_y;
  int m_particle_top_pt_ttbar_m;
  int m_particle_top_y_ttbar_pt;
  int m_particle_top_y_ttbar_y;
  int m_particle_top_y_ttbar_m;
  int m_particle_ttbar_pt_ttbar_y;
  int m_particle_ttbar_pt_ttbar_m;
  int m_particle_ttbar_y_ttbar_m;

  int m_reco_is_tau;
  int m_parton_is_tau;
  int m_particle_is_tau;

  //// Variables for Setting2

  float m2_b_pt;
  float m2_b_px;
  float m2_b_py;
  float m2_b_pz;
  float m2_b_eta;
  float m2_b_phi;
  float m2_b_e;
  float m2_b_m;
  float m2_b_y;

  float m2_bbar_pt;
  float m2_bbar_px;
  float m2_bbar_py;
  float m2_bbar_pz;
  float m2_bbar_eta;
  float m2_bbar_phi;
  float m2_bbar_e;
  float m2_bbar_m;
  float m2_bbar_y;

  float m2_top_pt;
  float m2_top_eta;
  float m2_top_phi;
  float m2_top_e;
  float m2_top_m;
  float m2_top_y;
  float m2_top_ratio;

  float m2_tbar_pt;
  float m2_tbar_eta;
  float m2_tbar_phi;
  float m2_tbar_e;
  float m2_tbar_m;
  float m2_tbar_y;
  float m2_tbar_ratio;
  
  float m2_ttbar_pt;
  float m2_ttbar_eta;
  float m2_ttbar_phi;
  float m2_ttbar_e;
  float m2_ttbar_m;
  float m2_ttbar_y;
  float m2_ttbar_coscos;

  // bbbar_prime refers to the bbbar pair not connected to the top decays   
  
 
  float m2_b_prime_pt;
  float m2_b_prime_px;
  float m2_b_prime_py;
  float m2_b_prime_pz;
  float m2_b_prime_eta;
  float m2_b_prime_phi;
  float m2_b_prime_e;
  float m2_b_prime_m;
  float m2_b_prime_y;

  float m2_bbar_prime_pt;
  float m2_bbar_prime_px;
  float m2_bbar_prime_py;
  float m2_bbar_prime_pz;
  float m2_bbar_prime_eta;
  float m2_bbar_prime_phi;
  float m2_bbar_prime_e;
  float m2_bbar_prime_m;
  float m2_bbar_prime_y;



  float m2_bbbar_prime_pt;
  float m2_bbbar_prime_eta;
  float m2_bbbar_prime_phi;
  //float m2_bbbar_e;
  float m2_bbbar_prime_m;
  //float m2_bbbar_y;
  //float m2_bbbar_coscos;

  float m2_bbbar_prime_delta_pt;
  float m2_bbbar_prime_delta_eta;
  float m2_bbbar_prime_delta_phi;
  //float m2_bbbar_e;
  float m2_bbbar_prime_dR;
  float m2_bbbar_prime_dy;
  float m2_bbbar_prime_dabsy;

  int m2_b_index;
  int m2_bbar_index;
  int m2_b_prime_index;
  int m2_bbar_prime_index;

  float m2_nu_pt;
  float m2_nu_eta;
  float m2_nu_phi;
  float m2_nu_e;
  float m2_nu_m;
  float m2_nu_y;

  float m2_nubar_pt;
  float m2_nubar_eta;
  float m2_nubar_phi;
  float m2_nubar_e;
  float m2_nubar_m;
  float m2_nubar_y;

  float m2_Wp_pt;
  float m2_Wp_eta;
  float m2_Wp_phi;
  float m2_Wp_e;
  float m2_Wp_m;
  float m2_Wp_y;

  float m2_Wm_pt;
  float m2_Wm_eta;
  float m2_Wm_phi;
  float m2_Wm_e;
  float m2_Wm_m;
  float m2_Wm_y;

  //// End Variables for Setting2
  float m_weight_max;
  float m2_weight_max;
  std::vector<float> m_weight_all;
  std::vector<float> m2_weight_all;
  TRandom3 m_random;
  TRandom3 m_random_2;

  //ClassDef(top::TopDileptonEventSaver, 1);
  ClassDef(TopDileptonEventSaver, 0);
  };

  //}

#endif
