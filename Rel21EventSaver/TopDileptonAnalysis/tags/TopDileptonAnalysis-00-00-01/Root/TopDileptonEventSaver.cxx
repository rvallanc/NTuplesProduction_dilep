#include "TopDileptonAnalysis/TopDileptonEventSaver.h"
#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopEventSelectionTools/TreeManager.h"
#include "TopParticleLevel/ParticleLevelEvent.h"
#include "TopConfiguration/TopConfig.h"
#include "TopEventSelectionTools/PlotManager.h"

#include "TopEvent/TopEventMaker.h"
#include "TopEvent/Event.h"

#include "TH1.h"
#include "TH2.h"
#include "TopDileptonAnalysis/NeutrinoWeighter.h"

//namespace top {

///-- Always initialise primitive types in the constructor --///
TopDileptonEventSaver::TopDileptonEventSaver() : 
  is_MC(true),
  m_NominalTreeManager(nullptr),
  m_truthTreeManager(nullptr)
{ 
}


  //TopDileptonEventSaver::~TopDileptonEventSaver() {
  //}

void TopDileptonEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file,const std::vector<std::string>& extraBranches) {

  m_config = config;
  EventSaverFlatNtuple::initialize(config, file, extraBranches);

  if (config->isMC()){
    is_MC = true;
    m_truthTreeManager = EventSaverFlatNtuple::truthTreeManager();
  } else {
    is_MC = false;
  }
  is_4bjets = false;

  ///////////////////////////////////////////////////////////
  ///-- Add our extra variable to each systematic TTree --///
  ///////////////////////////////////////////////////////////

  for (auto systematicTree : treeManagers()) {

    ///-- Event Info --///
    systematicTree->makeOutputVariable(m_fakeEvent,                     "d_fakeEvent");

    ///-- Flags --///
    systematicTree->makeOutputVariable(m_pass_emu,                      "d_pass_emu");
    systematicTree->makeOutputVariable(m_pass_emu_pretag,               "d_pass_emu_pretag");
    systematicTree->makeOutputVariable(m_pass_control_emu,              "d_pass_control_emu");
    systematicTree->makeOutputVariable(m_reco_is_tau,                   "d_is_tau");    

    systematicTree->makeOutputVariable(m_jet_n,                         "d_jet_n");
    systematicTree->makeOutputVariable(m_jet_pt,                        "d_jet_pt");
    systematicTree->makeOutputVariable(m_jet_eta,                       "d_jet_eta");
    systematicTree->makeOutputVariable(m_jet_phi,                       "d_jet_phi");
    systematicTree->makeOutputVariable(m_jet_e,                         "d_jet_e");
    systematicTree->makeOutputVariable(m_jet_jvt,                       "d_jet_jvt");
    systematicTree->makeOutputVariable(m_jet_jvc,                       "d_jet_jvc");
    systematicTree->makeOutputVariable(m_jet_truth,                     "d_jet_truth");
           
    systematicTree->makeOutputVariable(m_bjet_n,                        "d_bjet_n");
    systematicTree->makeOutputVariable(m_bjet_n_85,                     "d_bjet_n_85");
    systematicTree->makeOutputVariable(m_bjet_n_77,                     "d_bjet_n_77");
    systematicTree->makeOutputVariable(m_bjet_n_70,                     "d_bjet_n_70");
    systematicTree->makeOutputVariable(m_bjet_n_60,                     "d_bjet_n_60");

    systematicTree->makeOutputVariable(m_bjet_pt,                       "d_bjet_pt");
    systematicTree->makeOutputVariable(m_bjet_eta,                      "d_bjet_eta");
    systematicTree->makeOutputVariable(m_bjet_phi,                      "d_bjet_phi");
    systematicTree->makeOutputVariable(m_bjet_e,                        "d_bjet_e");
    systematicTree->makeOutputVariable(m_bjet_jvt,                      "d_bjet_jvt");    
    systematicTree->makeOutputVariable(m_bjet_jvc,                      "d_bjet_jvc");    

    systematicTree->makeOutputVariable(m_lep_n,                         "d_lep_n");
    systematicTree->makeOutputVariable(m_lep_pt,                        "d_lep_pt");
    systematicTree->makeOutputVariable(m_lep_eta,                       "d_lep_eta");
    systematicTree->makeOutputVariable(m_lep_phi,                       "d_lep_phi");
    systematicTree->makeOutputVariable(m_lep_e,                         "d_lep_e");
    systematicTree->makeOutputVariable(m_lep_type,                      "d_lep_type");
    systematicTree->makeOutputVariable(m_lep_charge,                    "d_lep_charge");
    systematicTree->makeOutputVariable(m_lep_iso_gradient,              "d_lep_iso_gradient");
    systematicTree->makeOutputVariable(m_lep_iso_gradient_loose,        "d_lep_iso_gradient_loose");
    systematicTree->makeOutputVariable(m_lep_id_medium,                 "d_lep_id_medium");
    systematicTree->makeOutputVariable(m_lep_id_tight,                  "d_lep_id_tight");

    systematicTree->makeOutputVariable(m_dilep_m,                       "d_dilep_m");
    systematicTree->makeOutputVariable(m_dilep_pt,                      "d_dilep_pt");
    systematicTree->makeOutputVariable(m_dilep_delta_phi,               "d_dilep_delta_phi");
    systematicTree->makeOutputVariable(m_dilep_delta_eta,               "d_dilep_delta_eta");
    systematicTree->makeOutputVariable(m_mlb,                           "d_mlb");
    systematicTree->makeOutputVariable(m_met_et,                        "d_met_et");
    systematicTree->makeOutputVariable(m_met_phi,                       "d_met_phi");
    systematicTree->makeOutputVariable(m_met_ex,                        "d_met_ex");
    systematicTree->makeOutputVariable(m_met_ey,                        "d_met_ey");
    systematicTree->makeOutputVariable(m_met_sumet,                     "d_met_sumet");

    ///-- ttbar reconstructon info --///
    systematicTree->makeOutputVariable(m_weight_max,                    "d_weight_max");

    systematicTree->makeOutputVariable(m_b_pt,                          "d_b_pt");
    systematicTree->makeOutputVariable(m_b_eta,                         "d_b_eta");
    systematicTree->makeOutputVariable(m_b_phi,                         "d_b_phi");
    systematicTree->makeOutputVariable(m_b_e,                           "d_b_e");
    systematicTree->makeOutputVariable(m_b_m,                           "d_b_m");
    systematicTree->makeOutputVariable(m_b_y,                           "d_b_y");

    systematicTree->makeOutputVariable(m_bbar_pt,                       "d_bbar_pt");
    systematicTree->makeOutputVariable(m_bbar_eta,                      "d_bbar_eta");
    systematicTree->makeOutputVariable(m_bbar_phi,                      "d_bbar_phi");
    systematicTree->makeOutputVariable(m_bbar_e,                        "d_bbar_e");
    systematicTree->makeOutputVariable(m_bbar_m,                        "d_bbar_m");
    systematicTree->makeOutputVariable(m_bbar_y,                        "d_bbar_y");

    systematicTree->makeOutputVariable(m_top_pt,                        "d_top_pt");
    systematicTree->makeOutputVariable(m_top_eta,                       "d_top_eta");
    systematicTree->makeOutputVariable(m_top_phi,                       "d_top_phi");
    systematicTree->makeOutputVariable(m_top_e,                         "d_top_e");
    systematicTree->makeOutputVariable(m_top_m,                         "d_top_m");
    systematicTree->makeOutputVariable(m_top_y,                         "d_top_y");

    systematicTree->makeOutputVariable(m_tbar_pt,                       "d_tbar_pt");
    systematicTree->makeOutputVariable(m_tbar_eta,                      "d_tbar_eta");
    systematicTree->makeOutputVariable(m_tbar_phi,                      "d_tbar_phi");
    systematicTree->makeOutputVariable(m_tbar_e,                        "d_tbar_e");
    systematicTree->makeOutputVariable(m_tbar_m,                        "d_tbar_m");
    systematicTree->makeOutputVariable(m_tbar_y,                        "d_tbar_y");

    systematicTree->makeOutputVariable(m_av_top_pt,                        "d_av_top_pt");
    systematicTree->makeOutputVariable(m_av_top_eta,                       "d_av_top_eta");
    systematicTree->makeOutputVariable(m_av_top_phi,                       "d_av_top_phi");
    systematicTree->makeOutputVariable(m_av_top_e,                         "d_av_top_e");
    systematicTree->makeOutputVariable(m_av_top_m,                         "d_av_top_m");
    systematicTree->makeOutputVariable(m_av_top_y,                         "d_av_top_y");

    systematicTree->makeOutputVariable(m_t1_pt,                        "d_t1_pt");
    systematicTree->makeOutputVariable(m_t1_eta,                       "d_t1_eta");
    systematicTree->makeOutputVariable(m_t1_phi,                       "d_t1_phi");
    systematicTree->makeOutputVariable(m_t1_e,                         "d_t1_e");
    systematicTree->makeOutputVariable(m_t1_m,                         "d_t1_m");
    systematicTree->makeOutputVariable(m_t1_y,                         "d_t1_y");

    systematicTree->makeOutputVariable(m_t2_pt,                        "d_t2_pt");
    systematicTree->makeOutputVariable(m_t2_eta,                       "d_t2_eta");
    systematicTree->makeOutputVariable(m_t2_phi,                       "d_t2_phi");
    systematicTree->makeOutputVariable(m_t2_e,                         "d_t2_e");
    systematicTree->makeOutputVariable(m_t2_m,                         "d_t2_m");
    systematicTree->makeOutputVariable(m_t2_y,                         "d_t2_y");

    systematicTree->makeOutputVariable(m_ttbar_pt,                      "d_ttbar_pt");
    systematicTree->makeOutputVariable(m_ttbar_eta,                     "d_ttbar_eta");
    systematicTree->makeOutputVariable(m_ttbar_phi,                     "d_ttbar_phi");
    systematicTree->makeOutputVariable(m_ttbar_e,                       "d_ttbar_e");
    systematicTree->makeOutputVariable(m_ttbar_m,                       "d_ttbar_m");
    systematicTree->makeOutputVariable(m_ttbar_y,                       "d_ttbar_y");
    systematicTree->makeOutputVariable(m_ttbar_dphi,     		"d_ttbar_dphi");
    systematicTree->makeOutputVariable(m_ttbar_pout,	        	"d_ttbar_pout");

    systematicTree->makeOutputVariable(m_nu_pt,                         "d_nu_pt");
    systematicTree->makeOutputVariable(m_nu_eta,                        "d_nu_eta");
    systematicTree->makeOutputVariable(m_nu_phi,                        "d_nu_phi");
    systematicTree->makeOutputVariable(m_nu_e,                          "d_nu_e");
    systematicTree->makeOutputVariable(m_nu_m,                          "d_nu_m");
    systematicTree->makeOutputVariable(m_nu_y,                          "d_nu_y");

    systematicTree->makeOutputVariable(m_nubar_pt,                      "d_nubar_pt");
    systematicTree->makeOutputVariable(m_nubar_eta,                     "d_nubar_eta");
    systematicTree->makeOutputVariable(m_nubar_phi,                     "d_nubar_phi");
    systematicTree->makeOutputVariable(m_nubar_e,                       "d_nubar_e");
    systematicTree->makeOutputVariable(m_nubar_m,                       "d_nubar_m");
    systematicTree->makeOutputVariable(m_nubar_y,                       "d_nubar_y");

    systematicTree->makeOutputVariable(m_Wp_pt,                         "d_Wp_pt");
    systematicTree->makeOutputVariable(m_Wp_eta,                        "d_Wp_eta");
    systematicTree->makeOutputVariable(m_Wp_phi,                        "d_Wp_phi");
    systematicTree->makeOutputVariable(m_Wp_e,                          "d_Wp_e");
    systematicTree->makeOutputVariable(m_Wp_m,                          "d_Wp_m");
    systematicTree->makeOutputVariable(m_Wp_y,                          "d_Wp_y");    

    systematicTree->makeOutputVariable(m_Wm_pt,                         "d_Wm_pt");
    systematicTree->makeOutputVariable(m_Wm_eta,                        "d_Wm_eta");
    systematicTree->makeOutputVariable(m_Wm_phi,                        "d_Wm_phi");
    systematicTree->makeOutputVariable(m_Wm_e,                          "d_Wm_e");
    systematicTree->makeOutputVariable(m_Wm_m,                          "d_Wm_m");
    systematicTree->makeOutputVariable(m_Wm_y,                          "d_Wm_y");    

    systematicTree->makeOutputVariable(m_jet_from_top_index,            "d_jet_from_top_index");
    systematicTree->makeOutputVariable(m_jet_from_tbar_index,           "d_jet_from_tbar_index");
    systematicTree->makeOutputVariable(m_non_top_mass,                  "d_non_top_mass");

    systematicTree->makeOutputVariable(m_reco_cos_helicity_p,           "d_cos_helicity_p");
    systematicTree->makeOutputVariable(m_reco_cos_helicity_m,           "d_cos_helicity_m");
    systematicTree->makeOutputVariable(m_reco_cos_raxis_p,              "d_cos_raxis_p");
    systematicTree->makeOutputVariable(m_reco_cos_raxis_m,              "d_cos_raxis_m");
    systematicTree->makeOutputVariable(m_reco_cos_transverse_p,         "d_cos_transverse_p");
    systematicTree->makeOutputVariable(m_reco_cos_transverse_m,         "d_cos_transverse_m");
    systematicTree->makeOutputVariable(m_reco_opening_angle,            "d_opening_angle");
    systematicTree->makeOutputVariable(m_reco_phi_star_t_tbar,          "d_phi_star_t_tbar");


    if (is_4bjets){
      systematicTree->makeOutputVariable(m_b_prime_pt,                          "d_b_prime_pt");
      systematicTree->makeOutputVariable(m_b_prime_px,                          "d_b_prime_px");
      systematicTree->makeOutputVariable(m_b_prime_py,                          "d_b_prime_py");
      systematicTree->makeOutputVariable(m_b_prime_pz,                          "d_b_prime_pz");
      systematicTree->makeOutputVariable(m_b_prime_eta,                         "d_b_prime_eta");
      systematicTree->makeOutputVariable(m_b_prime_phi,                         "d_b_prime_phi");
      systematicTree->makeOutputVariable(m_b_prime_e,                           "d_b_prime_e");
      systematicTree->makeOutputVariable(m_b_prime_m,                           "d_b_prime_m");
      systematicTree->makeOutputVariable(m_b_prime_y,                           "d_b_prime_y");

      systematicTree->makeOutputVariable(m_bbar_prime_pt,                       "d_bbar_prime_pt");
      systematicTree->makeOutputVariable(m_bbar_prime_px,                       "d_bbar_prime_px");
      systematicTree->makeOutputVariable(m_bbar_prime_py,                       "d_bbar_prime_py");
      systematicTree->makeOutputVariable(m_bbar_prime_pz,                       "d_bbar_prime_pz");
      systematicTree->makeOutputVariable(m_bbar_prime_eta,                      "d_bbar_prime_eta");
      systematicTree->makeOutputVariable(m_bbar_prime_phi,                      "d_bbar_prime_phi");
      systematicTree->makeOutputVariable(m_bbar_prime_e,                        "d_bbar_prime_e");
      systematicTree->makeOutputVariable(m_bbar_prime_m,                        "d_bbar_prime_m");
      systematicTree->makeOutputVariable(m_bbar_prime_y,                        "d_bbar_prime_y");

      systematicTree->makeOutputVariable(m_bbbar_prime_pt,                "d_bbbar_prime_pt");
      systematicTree->makeOutputVariable(m_bbbar_prime_eta,               "d_bbbar_prime_eta");
      systematicTree->makeOutputVariable(m_bbbar_prime_phi,               "d_bbbar_prime_phi");
      systematicTree->makeOutputVariable(m_bbbar_prime_m,                 "d_bbbar_prime_m");
      systematicTree->makeOutputVariable(m_bbbar_prime_delta_pt,          "d_bbbar_prime_delta_pt");
      systematicTree->makeOutputVariable(m_bbbar_prime_delta_eta,         "d_bbbar_prime_delta_eta");
      systematicTree->makeOutputVariable(m_bbbar_prime_delta_phi,         "d_bbbar_prime_delta_phi");
      systematicTree->makeOutputVariable(m_bbbar_prime_dR,                "d_bbbar_prime_dR");
      systematicTree->makeOutputVariable(m_bbbar_prime_dy,                "d_bbbar_prime_dy");
      systematicTree->makeOutputVariable(m_bbbar_prime_dabsy,             "d_bbbar_prime_dabsy");

      systematicTree->makeOutputVariable(m_weight_all,                    "d_weight_all");

      systematicTree->makeOutputVariable(m_b_prime_index,                 "d_b_prime_index");
      systematicTree->makeOutputVariable(m_bbar_prime_index,              "d_bbar_prime_index");
      systematicTree->makeOutputVariable(m_b_index,                       "d_b_index");
      systematicTree->makeOutputVariable(m_bbar_index,                    "d_bbar_index");
    }

    if(is_MC){
      systematicTree->makeOutputVariable(m_lep_truth_type,              "d_lep_truth_type");
    }

  } /// End of tree-manager loop 
  if(is_MC){
    setupPartonLevelEvent();
  }

  if ( topConfig()->doTopParticleLevel() ){
    //EventSaverFlatNtuple::setupParticleLevelTreeManager(plEvent);                                                                                            

    particleLevelTreeManager()->makeOutputVariable(m_particle_pass_emu,             "d_pass_emu");
    particleLevelTreeManager()->makeOutputVariable(m_particle_is_tau,               "d_is_tau");    

    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_n,                "d_lep_n");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_pt,               "d_lep_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_eta,              "d_lep_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_phi,              "d_lep_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_e,                "d_lep_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_type,             "d_lep_type");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_charge,           "d_lep_charge");

    particleLevelTreeManager()->makeOutputVariable(m_particle_jet_n,                "d_jet_n");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_n,               "d_bjet_n");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_pt,              "d_bjet_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_eta,             "d_bjet_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_phi,             "d_bjet_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_e,               "d_bjet_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_ghosts,          "d_bjet_ghosts");
    particleLevelTreeManager()->makeOutputVariable(m_particle_b_index,              "d_b_index");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bbar_index,           "d_bbar_index");          

    particleLevelTreeManager()->makeOutputVariable(m_particle_dilep_m,              "d_dilep_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_dilep_pt,             "d_dilep_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_dilep_delta_phi,      "d_dilep_delta_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_dilep_delta_eta,      "d_dilep_delta_eta");

    particleLevelTreeManager()->makeOutputVariable(m_particle_met_et,               "d_met_et");
    particleLevelTreeManager()->makeOutputVariable(m_particle_met_phi,              "d_met_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_met_ex,               "d_met_ex");
    particleLevelTreeManager()->makeOutputVariable(m_particle_met_ey,               "d_met_ey");

    particleLevelTreeManager()->makeOutputVariable(m_particle_top_pt,               "d_top_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_top_eta,              "d_top_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_top_phi,              "d_top_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_top_e,                "d_top_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_top_m,                "d_top_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_top_y,                "d_top_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_pt,              "d_tbar_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_eta,             "d_tbar_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_phi,             "d_tbar_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_e,               "d_tbar_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_m,               "d_tbar_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_y,               "d_tbar_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_pt,            "d_av_top_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_eta,           "d_av_top_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_phi,           "d_av_top_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_e,             "d_av_top_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_m,             "d_av_top_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_y,             "d_av_top_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_pt,               "d_t1_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_eta,              "d_t1_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_phi,              "d_t1_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_e,                "d_t1_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_m,                "d_t1_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_y,                "d_t1_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_pt,               "d_t2_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_eta,              "d_t2_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_phi,              "d_t2_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_e,                "d_t2_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_m,                "d_t2_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_y,                "d_t2_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_pt,             "d_ttbar_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_eta,            "d_ttbar_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_phi,            "d_ttbar_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_e,              "d_ttbar_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_m,              "d_ttbar_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_y,              "d_ttbar_y");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_dphi,	    "d_ttbar_dphi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_pout,	    "d_ttbar_pout");

    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_helicity_p,           "d_cos_helicity_p");
    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_helicity_m,           "d_cos_helicity_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_raxis_p,              "d_cos_raxis_p");
    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_raxis_m,              "d_cos_raxis_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_transverse_p,         "d_cos_transverse_p");
    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_transverse_m,         "d_cos_transverse_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_opening_angle,            "d_opening_angle");
    particleLevelTreeManager()->makeOutputVariable(m_particle_phi_star_t_tbar,          "d_phi_star_t_tbar");

    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_n,                 "d_nu_n");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_pt,                "d_nu_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_eta,               "d_nu_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_phi,               "d_nu_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_e,                 "d_nu_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_m,                 "d_nu_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_y,                 "d_nu_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_pt,             "d_nubar_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_eta,            "d_nubar_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_phi,            "d_nubar_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_e,              "d_nubar_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_m,              "d_nubar_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_y,              "d_nubar_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_pt,                "d_Wp_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_eta,               "d_Wp_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_phi,               "d_Wp_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_e,                 "d_Wp_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_m,                 "d_Wp_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_y,                 "d_Wp_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_pt,                "d_Wm_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_eta,               "d_Wm_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_phi,               "d_Wm_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_e,                 "d_Wm_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_m,                 "d_Wm_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_y,                 "d_Wm_y");

  }
}

void TopDileptonEventSaver::saveEvent(const top::Event& event){

  initializeRecoLevelBranches();

  TLorentzVector lep_pos, lep_neg, jet_1, jet_2, jet_3, jet_4, top, tbar, ttbar, nu, nubar, b_prime, bbar_prime;

  bool jet_1_set   = false;
  bool jet_2_set   = false;
  bool jet_3_set   = false;
  bool jet_4_set   = false;
  bool lep_pos_set = false;
  bool lep_neg_set = false;

  ///////////////////////////////////////
  ///--       Lepton Selection      --///
  ///////////////////////////////////////

  ///-- Gonna need the primary vertex for some lepton selections --///
  const xAOD::VertexContainer* m_primary_vertices = event.m_primaryVertices;
  int n_vertices = 0;
  float primary_vertex_z = 0.;
  for (const auto* const vertex: *m_primary_vertices) {
    const xAOD::VxType::VertexType vtype = vertex->vertexType();
    const int vmult = vertex->trackParticleLinks().size();
    // count vertices of type 1 (primary) and 3 (pileup) with >= 5 tracks
    if ((vtype == 1 || vtype == 3) && vmult >= 5) {
      ++n_vertices;
      // assuming there is only one primary vertex
      if (vtype == 1) primary_vertex_z = vertex->z();
    }
  }						       

  ///-- Electrons --///
  for (const auto* const electron : event.m_electrons) {    
    m_lep_pt.push_back(electron->pt()/1000.);
    m_lep_eta.push_back(electron->eta());
    m_lep_phi.push_back(electron->phi());
    m_lep_e.push_back(electron->e()/1000.);
    m_lep_type.push_back(11);
    m_lep_charge.push_back(electron->charge());

    if( electron->charge() > 0){
      lep_pos.SetPtEtaPhiE(electron->pt()/1000., electron->eta(), electron->phi(), electron->e()/1000.);
      lep_pos_set = true;
    } else if ( electron->charge() < 0){
      lep_neg.SetPtEtaPhiE(electron->pt()/1000., electron->eta(), electron->phi(), electron->e()/1000.);      
      lep_neg_set = true;
    } else {
      std::cout << "WARNING: Electron has no charge" << std::endl;
    }

    m_lep_iso_gradient.push_back(       int(electron->auxdataConst<char>("AnalysisTop_Isol_Gradient")));
    m_lep_iso_gradient_loose.push_back( int(electron->auxdataConst<char>("AnalysisTop_Isol_GradientLoose")));
    m_lep_id_medium.push_back( char(electron->auxdataConst<char>("DFCommonElectronsLHMedium")));
    m_lep_id_tight.push_back ( char(electron->auxdataConst<char>("DFCommonElectronsLHTight")));

    if(is_MC){
      if(electron->isAvailable<int>("truthType")){
	m_lep_truth_type.push_back(electron->auxdata<int>("truthType"));
        if(electron->auxdata<int>("truthType") != 2) m_fakeEvent = true;
      } else {
	m_lep_truth_type.push_back(-99.);
      }
    } else {
      m_lep_truth_type.push_back(-99.);
    }

    ++m_lep_n;
  } // End of electrons

  ///-- Muons --///
  for (const auto* const muon : event.m_muons) {

    m_lep_pt.push_back(muon->pt()/1000.); //muon->auxdata< float >( "InnerDetectorPt" )/1000.);
    m_lep_eta.push_back(muon->eta());
    m_lep_phi.push_back(muon->phi());
    m_lep_e.push_back(muon->e()/1000.);
    m_lep_type.push_back(13);
    m_lep_charge.push_back(muon->charge());

    if( muon->charge() > 0){
      lep_pos.SetPtEtaPhiE(muon->pt()/1000., muon->eta(), muon->phi(), muon->e()/1000.);
      lep_pos_set = true;
    } else if ( muon->charge() < 0){
      lep_neg.SetPtEtaPhiE(muon->pt()/1000., muon->eta(), muon->phi(), muon->e()/1000.);      
      lep_neg_set = true;
    } else {
      std::cout << "WARNING: Muon has no charge" << std::endl;
    }

    m_lep_iso_gradient.push_back(       int(muon->auxdataConst<char>("AnalysisTop_Isol_Gradient")));
    m_lep_iso_gradient_loose.push_back( int(muon->auxdataConst<char>("AnalysisTop_Isol_GradientLoose")));
    
    if(is_MC){
      if (muon->primaryTrackParticle()){
	if(muon->primaryTrackParticle()->isAvailable<int>("truthType")){
	  m_lep_truth_type.push_back(muon->primaryTrackParticle()->auxdata<int>("truthType"));
	  if(muon->primaryTrackParticle()->auxdata<int>("truthType") != 6) m_fakeEvent = true;
	} else {
	  m_lep_truth_type.push_back(-99.);
	}
      } else {
	m_lep_truth_type.push_back(-99.);
      }
    }

    ++m_lep_n;
  }


  //////////////////////////
  ///--     Jets       --///
  //////////////////////////

  int jet_index;
  int jet_index_1      = -99;
  int jet_index_2      = -99;
  int bjet_index_1     = -99;
  int bjet_index_2     = -99;
  int jet_1_index      = -99;
  int jet_2_index      = -99;

  //int jet_index_3      = -99;
  //int jet_index_4      = -99;
  int b_index          = -99;
  int bbar_index       = -99;
  int b_prime_index    = -99;
  int bbar_prime_index = -99;
  //  int jet_3_index      = -99;
  //  int jet_4_index      = -99;

  //--- Jet Parameters ---//
  std::vector< TLorentzVector > non_tagged_jets;
  std::vector< TLorentzVector > tagged_jets;

  for (const auto* const jet : event.m_jets) {
    jet_index = m_jet_n-1;
    ++jet_n;

    TLorentzVector temp;
    temp.SetPtEtaPhiE(jet->pt()/1000.,jet->eta(),jet->phi(),jet->e()/1000.);

    int jet_parton = -99;
    bool status = jet->getAttribute<int>("PartonTruthLabelID", jet_parton);
    if(!status) jet_parton = -99;

    float jet_jvt = -99;
    if (jet->isAvailable<float>("AnalysisTop_JVT")) {
      jet_jvt = jet->auxdataConst<float>("AnalysisTop_JVT");
    }

     ///-- Save these jets --///
     ++m_jet_n;
     m_jet_pt.push_back(jet->pt()/1000.);
     m_jet_eta.push_back(jet->eta());
     m_jet_phi.push_back(jet->phi());
     m_jet_e.push_back(jet->e()/1000.);
     m_jet_jvt.push_back(jet_jvt);	
     m_jet_truth.push_back(jet_parton);

     //--- Get the btag info --///
     double mvx = -999; 
     jet->btagging()->MVx_discriminant("MV2c10", mvx);

     double  jvc = -999;
     jet->btagging()->MVx_discriminant("JetVertexCharge", jvc);
     //std::cout << "jvc = " << jvc << std::endl;
     m_jet_jvc.push_back(jvc);

     float WorkingPoint_85 = 0.1758475;
     float WorkingPoint_77 = 0.645925;
     float WorkingPoint_70 = 0.8244273;
     float WorkingPoint_60 = 0.934906;

     if(mvx > WorkingPoint_85)
       ++m_bjet_n_85;
     if(mvx > WorkingPoint_77)
       ++m_bjet_n_77;
     if(mvx > WorkingPoint_70)
       ++m_bjet_n_70;
     if(mvx > WorkingPoint_60)
       ++m_bjet_n_60;

     if(mvx > WorkingPoint_85){
       m_bjet_pt.push_back(jet->pt()/1000.);
       m_bjet_eta.push_back(jet->eta());
       m_bjet_phi.push_back(jet->phi());
       m_bjet_e.push_back(jet->e()/1000.);     
       m_bjet_jvt.push_back(jet_jvt); 
       m_bjet_jvc.push_back(jvc);
       ++m_bjet_n;
       tagged_jets.push_back(temp);
       if(bjet_index_1 == -99)      bjet_index_1 = jet_index;
       else if(bjet_index_2 == -99) bjet_index_2 = jet_index;
     } else {
       /// Save the non-tagged jets just in case ///
       non_tagged_jets.push_back(temp);
       if(jet_index_1 == -99) 
	 jet_index_1 = jet_index;
       else if(jet_index_2 == -99) 
	 jet_index_2 = jet_index;
     }
  }
  
  for (size_t i = 0; i < tagged_jets.size(); i++){
    double max = tagged_jets[0].Pt();
    if (tagged_jets[i].Pt() > max) {
      std::cout << "Non-first-jet max! " << i << std::endl;
    }
  }

  for (size_t i = 0; i < non_tagged_jets.size(); i++){
     double max = non_tagged_jets[0].Pt();
     if (non_tagged_jets[i].Pt() > max){
       std::cout << "Non-first-jet max! (non-tagged) " << i << std::endl;
     }
  }

  ///-- Now decide which jets we want to run the NuW with --///

  ///-- 2 or more b-jets - run with highest pT b-jets --///
  if (tagged_jets.size() > 1){
    jet_1 = tagged_jets[0];
    jet_2 = tagged_jets[1];
    jet_1_set = true;
    jet_2_set = true;
    jet_1_index = bjet_index_1;
    jet_2_index = bjet_index_2;

    m_jet_from_top_index = bjet_index_1;
    m_jet_from_tbar_index = bjet_index_2;
  }
 
  ///-- If there's only 1 b-jet, run with b-tagged jet and highest pT non-tagged jet --///
  else if (tagged_jets.size() > 0  && non_tagged_jets.size() > 0){
    jet_1 = tagged_jets[0];
    jet_2 = non_tagged_jets[0];
    jet_1_set = true;
    jet_2_set = true;
    jet_1_index = bjet_index_1;
    jet_2_index = jet_index_1;
  }

  ///-- Let's also store the extra b-jets invariant mass for Zbb and Hbb --///
  if( tagged_jets.size() > 3){
    TLorentzVector bbbar = tagged_jets[2] + tagged_jets[3];
    jet_3 = tagged_jets[2];
    jet_4 = tagged_jets[3];
    jet_3_set = true;
    jet_4_set = true;
    m_non_top_mass = bbbar.M();
  }

  //--- MET observables ---//
  m_met_et    = event.m_met->met()/1000.;
  m_met_phi   = event.m_met->phi();
  m_met_ex    = event.m_met->mpx()/1000.;
  m_met_ey    = event.m_met->mpy()/1000.;
  m_met_sumet = event.m_met->sumet()/1000.;
  // Can also have sumet() //                                                                      

  
  //--- Event Level observables ---//                                                              
  TLorentzVector dilepton = lep_pos + lep_neg;
  m_dilep_m         = dilepton.M();
  m_dilep_pt        = dilepton.Pt();
  m_dilep_delta_phi = lep_pos.DeltaPhi(lep_neg);
  m_dilep_delta_eta = fabs(lep_pos.Eta()-lep_neg.Eta());

  ///-- Work out event flags --///
  if(m_lep_type.size() == 2){
    m_pass_emu          = passEMu();
    m_pass_emu_pretag   = passEMu_pretag();
    m_pass_control_emu  = passEMu_control();      
  }
  m_reco_is_tau = m_parton_is_tau;

  ////////////////////////////////////
  ///--    Top Reconstruction    --///
  ////////////////////////////////////

  if( lep_pos_set && lep_neg_set && m_lep_n == 2 && jet_1_set && jet_2_set){

    TLorentzVector b, bbar;
    TLorentzVector m2_b, m2_bbar;

    ///-- Determine which pairing to use --///
    double deltaR_lep_pos_jet_1 = lep_pos.DeltaR(jet_1);
    double deltaR_lep_neg_jet_2 = lep_neg.DeltaR(jet_2);
    double distance_a = fabs(deltaR_lep_pos_jet_1 + deltaR_lep_neg_jet_2);
    
    double deltaR_lep_pos_jet_2 = lep_pos.DeltaR(jet_2);
    double deltaR_lep_neg_jet_1 = lep_neg.DeltaR(jet_1);
    double distance_b = fabs(deltaR_lep_pos_jet_2 + deltaR_lep_neg_jet_1);
    
    if( distance_a < distance_b){
      b    = jet_1;
      bbar = jet_2;
    } else if (distance_b < distance_a){
      b    = jet_2;
      bbar = jet_1;
    } else {
      std::cout << "WARNING: Could not determine lepton-b pairings" <<  std::endl;
      lep_pos.Print();
      lep_neg.Print();
      jet_1.Print();
      jet_2.Print();
      std::cout << "distance_a = " << distance_a << deltaR_lep_pos_jet_1 << deltaR_lep_neg_jet_2 << std::endl;
      std::cout << "distance_b = " << distance_b << deltaR_lep_pos_jet_2 << deltaR_lep_neg_jet_1 << std::endl;
      b    = jet_1;
      bbar = jet_2;
    }
    
    m_mlb.push_back((b + lep_pos).M());
    m_mlb.push_back((bbar + lep_neg).M());

    ///-- neutrino weighting --///
    top::NeutrinoWeighter nuW     = top::NeutrinoWeighter(1);//, lep_pos.Pt() + lep_pos.Phi());// Ingnore the second argument,it's just a random string
    // Loop over all
    if (!is_4bjets){
      top::NeutrinoWeighter nuW_alt = top::NeutrinoWeighter(1);//, lep_pos.Pt() + lep_pos.Eta() + m_bjet_n);//2
      m_weight_max  = nuW.Reconstruct(lep_pos, lep_neg, b, bbar, m_met_ex, m_met_ey, m_met_phi);
      double m_weight_max_alt = nuW_alt.Reconstruct(lep_pos, lep_neg, bbar, b, m_met_ex, m_met_ey, m_met_phi);
      
      if( m_weight_max_alt > m_weight_max){
	nuW = nuW_alt;
	m_weight_max = m_weight_max_alt;
      }
    } else {
      std::cout << "Looping through jets" << std::endl;
      for (int jet_a_counter = 0; jet_a_counter < 4; jet_a_counter++){
        for (int jet_b_counter = 0; jet_b_counter < 4; jet_b_counter++){
          if (jet_a_counter != jet_b_counter){
            if (jet_3_set && jet_4_set){
	      top::NeutrinoWeighter nuW_alt = top::NeutrinoWeighter(1);//, lep_neg.Eta() + lep_neg.Phi());//2
              double tmp_weight = nuW_alt.Reconstruct(lep_pos, lep_neg, tagged_jets[jet_a_counter], tagged_jets[jet_b_counter], m_met_ex, m_met_ey, m_met_phi);
              m_weight_all.push_back(tmp_weight);  
              if(tmp_weight > m_weight_max){
                m_weight_max = tmp_weight;
                b_index           = jet_a_counter;
                bbar_index        = jet_b_counter;
		
                b_prime_index     = (jet_a_counter + 1)%4 != jet_b_counter ? (jet_a_counter + 1)%4 : (jet_a_counter + 2)%4;
                bbar_prime_index  = (jet_a_counter + 3)%4 != jet_b_counter ? (jet_a_counter + 3)%4 : (jet_a_counter + 2)%4;
                nuW = nuW_alt;
	      }
            }
            else{
              m_weight_all.push_back(-1000);
            }
          }
        }
      } 
    }
    
    if(m_weight_max > 0.){
      top   = nuW.GetTop();
      tbar  = nuW.GetTbar();
      ttbar = nuW.GetTtbar();
      b     = nuW.GetB();
      bbar  = nuW.GetBbar();
      nu    = nuW.GetNu();
      nubar = nuW.GetNubar();

      TLorentzVector Wp, Wm;
      Wp = lep_pos + nu;
      Wm = lep_neg + nubar;

      m_top_pt    = top.Pt();
      m_top_eta   = top.Eta();
      m_top_phi   = top.Phi();
      m_top_m     = top.M();
      m_top_e     = top.E();
      m_top_y     = top.Rapidity();
      
      m_tbar_pt   = tbar.Pt();
      m_tbar_eta  = tbar.Eta();
      m_tbar_phi  = tbar.Phi();
      m_tbar_m    = tbar.M();
      m_tbar_e    = tbar.E();
      m_tbar_y    = tbar.Rapidity();

      m_av_top_pt    = (m_top_pt  + m_tbar_pt)/2.;
      m_av_top_eta   = (m_top_eta + m_tbar_eta)/2.;
      m_av_top_phi   = (m_top_phi + m_tbar_phi)/2.;
      m_av_top_m     = (m_top_m   + m_tbar_m)/2.;
      m_av_top_e     = (m_top_e   + m_tbar_e)/2.;
      m_av_top_y     = (m_top_y   + m_tbar_y)/2.;

      if(m_top_pt > m_tbar_pt){
	m_t1_pt    = m_top_pt;
	m_t1_eta   = m_top_eta;
	m_t1_phi   = m_top_phi;
	m_t1_m     = m_top_m;
	m_t1_e     = m_top_e;
	m_t1_y     = m_top_y;
	m_t2_pt    = m_tbar_pt;
	m_t2_eta   = m_tbar_eta;
	m_t2_phi   = m_tbar_phi;
	m_t2_m     = m_tbar_m;
	m_t2_e     = m_tbar_e;
	m_t2_y     = m_tbar_y;
      } else{
	m_t2_pt    = m_top_pt;
	m_t2_eta   = m_top_eta;
	m_t2_phi   = m_top_phi;
	m_t2_m     = m_top_m;
	m_t2_e     = m_top_e;
	m_t2_y     = m_top_y;
	m_t1_pt    = m_tbar_pt;
	m_t1_eta   = m_tbar_eta;
	m_t1_phi   = m_tbar_phi;
	m_t1_m     = m_tbar_m;
	m_t1_e     = m_tbar_e;
	m_t1_y     = m_tbar_y;
      }

      m_ttbar_pt   = ttbar.Pt();
      m_ttbar_eta  = ttbar.Eta();
      m_ttbar_phi  = ttbar.Phi();
      m_ttbar_m    = ttbar.M();
      m_ttbar_e    = ttbar.E();
      m_ttbar_y    = ttbar.Rapidity();
      m_ttbar_dphi = calculateDphi_ttbar( top, tbar );
      m_ttbar_pout = calculatePout( top, tbar );

      m_reco_cos_helicity_p   = cos_theta_helicity(   top, top,  ttbar, lep_pos, +1);
      m_reco_cos_helicity_m   = cos_theta_helicity(   top, tbar, ttbar, lep_neg, -1);
      m_reco_cos_raxis_p      = cos_theta_raxis(      top, top,  ttbar, lep_pos, +1);
      m_reco_cos_raxis_m      = cos_theta_raxis(      top, tbar, ttbar, lep_neg, -1);
      m_reco_cos_transverse_p = cos_theta_transverse( top, top,  ttbar, lep_pos, +1);
      m_reco_cos_transverse_m = cos_theta_transverse( top, tbar, ttbar, lep_neg, -1);
      m_reco_opening_angle    = opening_angle(        ttbar, lep_neg, lep_pos);
      m_reco_phi_star_t_tbar  = phi_star(             top, tbar); 




      m_nu_pt     = nu.Pt();
      m_nu_eta    = nu.Eta();
      m_nu_phi    = nu.Phi();
      m_nu_m      = nu.M();
      m_nu_e      = nu.E();
      m_nu_y      = nu.Rapidity();
      
      m_nubar_pt  = nubar.Pt();
      m_nubar_eta = nubar.Eta();
      m_nubar_phi = nubar.Phi();
      m_nubar_m   = nubar.M();
      m_nubar_e   = nubar.E();
      m_nubar_y   = nubar.Rapidity();

      m_b_pt      = b.Pt();
      m_b_eta     = b.Eta();
      m_b_phi     = b.Phi();
      m_b_m       = b.M();
      m_b_e       = b.E();
      m_b_y       = b.Rapidity();
      
      m_bbar_pt   = bbar.Pt();
      m_bbar_eta  = bbar.Eta();
      m_bbar_phi  = bbar.Phi();
      m_bbar_m    = bbar.M();
      m_bbar_e    = bbar.E();
      m_bbar_y    = bbar.Rapidity();

      m_Wp_pt     = Wp.Pt();
      m_Wp_eta    = Wp.Eta();
      m_Wp_phi    = Wp.Phi();
      m_Wp_m      = Wp.M();
      m_Wp_e      = Wp.E();
      m_Wp_y      = Wp.Rapidity();

      m_Wm_pt     = Wm.Pt();
      m_Wm_eta    = Wm.Eta();
      m_Wm_phi    = Wm.Phi();
      m_Wm_m      = Wm.M();
      m_Wm_e      = Wm.E();
      m_Wm_y      = Wm.Rapidity();

      if (is_4bjets){
        b_prime     = tagged_jets[b_prime_index];
        bbar_prime  = tagged_jets[bbar_prime_index]; 
        TLorentzVector bbbar = b_prime + bbar_prime;
	
        m_b_prime_pt      = b_prime.Pt();
        m_b_prime_px      = b_prime.Px();
        m_b_prime_py      = b_prime.Py();
        m_b_prime_pz      = b_prime.Pz();
        m_b_prime_eta     = b_prime.Eta();
        m_b_prime_phi     = b_prime.Phi();
        m_b_prime_m       = b_prime.M();
        m_b_prime_e       = b_prime.E();
        m_b_prime_y       = b_prime.Rapidity();
        
        m_bbar_prime_pt   = bbar_prime.Pt();
        m_bbar_prime_px   = bbar_prime.Px();
        m_bbar_prime_py   = bbar_prime.Py();
        m_bbar_prime_pz   = bbar_prime.Pz();
        m_bbar_prime_eta  = bbar_prime.Eta();
        m_bbar_prime_phi  = bbar_prime.Phi();
        m_bbar_prime_m    = bbar_prime.M();
        m_bbar_prime_e    = bbar_prime.E();
        m_bbar_prime_y    = bbar_prime.Rapidity();

        m_bbbar_prime_pt        = bbbar.Pt();
        m_bbbar_prime_eta       = bbbar.Eta();
        m_bbbar_prime_phi       = bbbar.Phi();
        m_bbbar_prime_m         = bbbar.M();
        m_bbbar_prime_delta_pt  = b_prime.Pt()-bbar_prime.Pt();
        m_bbbar_prime_delta_eta = b_prime.Eta()-bbar_prime.Eta();
        m_bbbar_prime_delta_phi = b_prime.Phi()-bbar_prime.Phi();
        m_bbbar_prime_dR        = b_prime.DeltaR(bbar_prime);
        m_bbbar_prime_dy        = b_prime.Rapidity()-bbar_prime.Rapidity();
        m_bbbar_prime_dabsy     = fabs(b_prime.Rapidity())-fabs(bbar_prime.Rapidity());
	
        m_b_prime_index = b_prime_index;
        m_bbar_prime_index = bbar_prime_index;
        m_b_index = b_index;
        m_bbar_index = bbar_index;
      } 
    }
  }
  ///-- Oh yeah, we want to set all the original variables too and the call fill on the TTree --///
  EventSaverFlatNtuple::saveEvent(event);
}


bool TopDileptonEventSaver::passEMu(){

  if (m_lep_n < 2 || m_jet_n < 2)
    return false;
  if (!(m_lep_type[0] == 11 && m_lep_type[1] == 13 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] >= 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5 || (abs(m_lep_eta[0]) < 1.52 && abs(m_lep_eta[0]) > 1.37))
    return false;
  if (abs(m_lep_eta[1]) > 2.5)
    return false;

  return true;
}


bool TopDileptonEventSaver::passEMu_pretag(){
  if (m_lep_n < 2)
    return false;
  if (!(m_lep_type[0] == 11 && m_lep_type[1] == 13 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] >= 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5 || (abs(m_lep_eta[0]) < 1.52 && abs(m_lep_eta[0]) > 1.37))
    return false;
  if (abs(m_lep_eta[1]) > 2.5)
    return false;

  return true;
}


bool TopDileptonEventSaver::passEMu_control(){
  if (m_lep_n < 2)
    return false;
  if (!(m_lep_type[0] == 11 && m_lep_type[1] == 13 ))
    return false;
  if (m_lep_iso_gradient[0] == 0 || m_lep_iso_gradient[1] == 0)
    return false;
  if (m_lep_charge[0] * m_lep_charge[1] < 0)
    return false;
  if (m_lep_pt[0] < 25. || m_lep_pt[1] < 25.)
    return false;
  if (abs(m_lep_eta[0]) > 2.5 || (abs(m_lep_eta[0]) < 1.52 && abs(m_lep_eta[0]) > 1.37))
    return false;
  if (abs(m_lep_eta[1]) > 2.5)
    return false;

  return true;
}
   
int TopDileptonEventSaver::findBin(float var, float *bins, int size){
  int bin = -100;
  for (int i = 1; i <= size; i++){
    if (var > bins[i-1]) bin = i;
  }
  return bin;
}

/*void TopDileptonEventSaver::setupParticleLevelTreeManager(const top::ParticleLevelEvent& plEvent) {

  if ( topConfig()->doTopParticleLevel() ){
    //EventSaverFlatNtuple::setupParticleLevelTreeManager(plEvent);                                                                                            

    particleLevelTreeManager()->makeOutputVariable(m_particle_pass_emu,             "d_pass_emu");
    particleLevelTreeManager()->makeOutputVariable(m_particle_is_tau,               "d_is_tau");    

    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_n,                "d_lep_n");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_pt,               "d_lep_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_eta,              "d_lep_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_phi,              "d_lep_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_e,                "d_lep_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_type,             "d_lep_type");
    particleLevelTreeManager()->makeOutputVariable(m_particle_lep_charge,           "d_lep_charge");

    particleLevelTreeManager()->makeOutputVariable(m_particle_jet_n,                "d_jet_n");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_n,               "d_bjet_n");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_pt,              "d_bjet_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_eta,             "d_bjet_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_phi,             "d_bjet_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_e,               "d_bjet_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bjet_ghosts,          "d_bjet_ghosts");
    particleLevelTreeManager()->makeOutputVariable(m_particle_b_index,              "d_b_index");
    particleLevelTreeManager()->makeOutputVariable(m_particle_bbar_index,           "d_bbar_index");          

    particleLevelTreeManager()->makeOutputVariable(m_particle_dilep_m,              "d_dilep_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_dilep_pt,             "d_dilep_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_dilep_delta_phi,      "d_dilep_delta_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_dilep_delta_eta,      "d_dilep_delta_eta");

    particleLevelTreeManager()->makeOutputVariable(m_particle_met_et,               "d_met_et");
    particleLevelTreeManager()->makeOutputVariable(m_particle_met_phi,              "d_met_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_met_ex,               "d_met_ex");
    particleLevelTreeManager()->makeOutputVariable(m_particle_met_ey,               "d_met_ey");

    particleLevelTreeManager()->makeOutputVariable(m_particle_top_pt,               "d_top_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_top_eta,              "d_top_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_top_phi,              "d_top_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_top_e,                "d_top_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_top_m,                "d_top_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_top_y,                "d_top_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_pt,              "d_tbar_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_eta,             "d_tbar_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_phi,             "d_tbar_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_e,               "d_tbar_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_m,               "d_tbar_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_tbar_y,               "d_tbar_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_pt,               "d_av_top_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_eta,              "d_av_top_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_phi,              "d_av_top_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_e,                "d_av_top_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_m,                "d_av_top_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_av_top_y,                "d_av_top_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_pt,               "d_t1_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_eta,              "d_t1_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_phi,              "d_t1_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_e,                "d_t1_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_m,                "d_t1_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t1_y,                "d_t1_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_pt,               "d_t2_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_eta,              "d_t2_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_phi,              "d_t2_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_e,                "d_t2_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_m,                "d_t2_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_t2_y,                "d_t2_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_pt,             "d_ttbar_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_eta,            "d_ttbar_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_phi,            "d_ttbar_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_e,              "d_ttbar_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_m,              "d_ttbar_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_y,              "d_ttbar_y");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_dphi,	    "d_ttbar_dphi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_ttbar_pout,	    "d_ttbar_pout");

    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_helicity_p,           "d_cos_helicity_p");
    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_helicity_m,           "d_cos_helicity_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_raxis_p,              "d_cos_raxis_p");
    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_raxis_m,              "d_cos_raxis_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_transverse_p,         "d_cos_transverse_p");
    particleLevelTreeManager()->makeOutputVariable(m_particle_cos_transverse_m,         "d_cos_transverse_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_opening_angle,            "d_opening_angle");
    particleLevelTreeManager()->makeOutputVariable(m_particle_phi_star_t_tbar,          "d_phi_star_t_tbar");


    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_n,                 "d_nu_n");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_pt,                "d_nu_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_eta,               "d_nu_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_phi,               "d_nu_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_e,                 "d_nu_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_m,                 "d_nu_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nu_y,                 "d_nu_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_pt,             "d_nubar_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_eta,            "d_nubar_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_phi,            "d_nubar_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_e,              "d_nubar_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_m,              "d_nubar_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_nubar_y,              "d_nubar_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_pt,                "d_Wp_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_eta,               "d_Wp_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_phi,               "d_Wp_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_e,                 "d_Wp_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_m,                 "d_Wp_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wp_y,                 "d_Wp_y");

    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_pt,                "d_Wm_pt");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_eta,               "d_Wm_eta");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_phi,               "d_Wm_phi");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_e,                 "d_Wm_e");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_m,                 "d_Wm_m");
    particleLevelTreeManager()->makeOutputVariable(m_particle_Wm_y,                 "d_Wm_y");

  }
  }*/

void TopDileptonEventSaver::setupPartonLevelEvent(){

  m_truthTreeManager->makeOutputVariable(m_parton_top_pt,          "d_top_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_top_eta,         "d_top_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_top_phi,         "d_top_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_top_e,           "d_top_e");
  m_truthTreeManager->makeOutputVariable(m_parton_top_m,           "d_top_m");
  m_truthTreeManager->makeOutputVariable(m_parton_top_y,           "d_top_y");
    
  m_truthTreeManager->makeOutputVariable(m_parton_tbar_pt,         "d_tbar_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_tbar_eta,        "d_tbar_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_tbar_phi,        "d_tbar_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_tbar_e,          "d_tbar_e");
  m_truthTreeManager->makeOutputVariable(m_parton_tbar_m,          "d_tbar_m");
  m_truthTreeManager->makeOutputVariable(m_parton_tbar_y,          "d_tbar_y");

  m_truthTreeManager->makeOutputVariable(m_parton_av_top_pt,       "d_av_top_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_av_top_eta,      "d_av_top_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_av_top_phi,      "d_av_top_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_av_top_e,        "d_av_top_e");
  m_truthTreeManager->makeOutputVariable(m_parton_av_top_m,        "d_av_top_m");
  m_truthTreeManager->makeOutputVariable(m_parton_av_top_y,        "d_av_top_y");
    
  m_truthTreeManager->makeOutputVariable(m_parton_t1_pt,          "d_t1_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_t1_eta,         "d_t1_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_t1_phi,         "d_t1_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_t1_e,           "d_t1_e");
  m_truthTreeManager->makeOutputVariable(m_parton_t1_m,           "d_t1_m");
  m_truthTreeManager->makeOutputVariable(m_parton_t1_y,           "d_t1_y");

  m_truthTreeManager->makeOutputVariable(m_parton_t2_pt,          "d_t2_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_t2_eta,         "d_t2_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_t2_phi,         "d_t2_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_t2_e,           "d_t2_e");
  m_truthTreeManager->makeOutputVariable(m_parton_t2_m,           "d_t2_m");
  m_truthTreeManager->makeOutputVariable(m_parton_t2_y,           "d_t2_y");

  m_truthTreeManager->makeOutputVariable(m_parton_ttbar_pt,        "d_ttbar_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_ttbar_eta,       "d_ttbar_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_ttbar_phi,       "d_ttbar_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_ttbar_e,         "d_ttbar_e");
  m_truthTreeManager->makeOutputVariable(m_parton_ttbar_m,         "d_ttbar_m");
  m_truthTreeManager->makeOutputVariable(m_parton_ttbar_y,         "d_ttbar_y");
  m_truthTreeManager->makeOutputVariable(m_parton_ttbar_dphi,	   "d_ttbar_dphi");
  m_truthTreeManager->makeOutputVariable(m_parton_ttbar_pout,      "d_ttbar_pout");
    
  m_truthTreeManager->makeOutputVariable(m_parton_cos_helicity_p,       "d_cos_helicity_p");
  m_truthTreeManager->makeOutputVariable(m_parton_cos_helicity_m,       "d_cos_helicity_m");
  m_truthTreeManager->makeOutputVariable(m_parton_cos_raxis_p,          "d_cos_raxis_p");
  m_truthTreeManager->makeOutputVariable(m_parton_cos_raxis_m,          "d_cos_raxis_m");
  m_truthTreeManager->makeOutputVariable(m_parton_cos_transverse_p,     "d_cos_transverse_p");
  m_truthTreeManager->makeOutputVariable(m_parton_cos_transverse_m,     "d_cos_transverse_m");
  m_truthTreeManager->makeOutputVariable(m_parton_opening_angle,        "d_opening_angle");
  m_truthTreeManager->makeOutputVariable(m_parton_phi_star_t_tbar,      "d_phi_star_t_tbar");


  m_truthTreeManager->makeOutputVariable(m_parton_l_pt,            "d_l_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_l_eta,           "d_l_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_l_phi,           "d_l_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_l_e,             "d_l_e");
  m_truthTreeManager->makeOutputVariable(m_parton_l_pdgId,         "d_l_pdgId");
    
  m_truthTreeManager->makeOutputVariable(m_parton_lbar_pt,         "d_lbar_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_lbar_eta,        "d_lbar_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_lbar_phi,        "d_lbar_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_lbar_e,          "d_lbar_e");
  m_truthTreeManager->makeOutputVariable(m_parton_lbar_pdgId,      "d_lbar_pdgId");

  m_truthTreeManager->makeOutputVariable(m_parton_nu_pt,            "d_nu_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_nu_eta,           "d_nu_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_nu_phi,           "d_nu_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_nu_e,             "d_nu_e");
  m_truthTreeManager->makeOutputVariable(m_parton_nu_pdgId,         "d_nu_pdgId");
    
  m_truthTreeManager->makeOutputVariable(m_parton_nubar_pt,         "d_nubar_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_nubar_eta,        "d_nubar_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_nubar_phi,        "d_nubar_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_nubar_e,          "d_nubar_e");
  m_truthTreeManager->makeOutputVariable(m_parton_nubar_pdgId,      "d_nubar_pdgId");

  m_truthTreeManager->makeOutputVariable(m_parton_dilep_delta_phi, "d_dilep_delta_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_dilep_delta_eta, "d_dilep_delta_eta");

  m_truthTreeManager->makeOutputVariable(m_parton_b_pt,            "d_b_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_b_eta,           "d_b_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_b_phi,           "d_b_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_b_e,             "d_b_e");
    
  m_truthTreeManager->makeOutputVariable(m_parton_bbar_pt,         "d_bbar_pt");
  m_truthTreeManager->makeOutputVariable(m_parton_bbar_eta,        "d_bbar_eta");
  m_truthTreeManager->makeOutputVariable(m_parton_bbar_phi,        "d_bbar_phi");
  m_truthTreeManager->makeOutputVariable(m_parton_bbar_e,          "d_bbar_e");
    
  m_truthTreeManager->makeOutputVariable(m_parton_weight_mc,       "d_weight_mc");
  m_truthTreeManager->makeOutputVariable(m_parton_eventNumber,     "d_eventNumber");
  m_truthTreeManager->makeOutputVariable(m_parton_runNumber,       "d_runNumber");

  m_truthTreeManager->makeOutputVariable(m_parton_is_tau,          "d_is_tau");

}

void TopDileptonEventSaver::initializePartonLevelEvent(){
  
  m_parton_top_pt          = -99.;
  m_parton_top_eta         = -99.;
  m_parton_top_phi         = -99.;
  m_parton_top_e           = -99.;
  m_parton_top_m           = -99.;
  m_parton_top_y           = -99.;
    
  m_parton_tbar_pt         = -99.;
  m_parton_tbar_eta        = -99.;
  m_parton_tbar_phi        = -99.;
  m_parton_tbar_e          = -99.;
  m_parton_tbar_m          = -99.;
  m_parton_tbar_y          = -99.;

  m_parton_av_top_pt       = -99.;
  m_parton_av_top_eta      = -99.;
  m_parton_av_top_phi      = -99.;
  m_parton_av_top_e        = -99.;
  m_parton_av_top_m        = -99.;
  m_parton_av_top_y        = -99.;

  m_parton_t1_pt          = -99.;
  m_parton_t1_eta         = -99.;
  m_parton_t1_phi         = -99.;
  m_parton_t1_e           = -99.;
  m_parton_t1_m           = -99.;
  m_parton_t1_y           = -99.;

  m_parton_t2_pt          = -99.;
  m_parton_t2_eta         = -99.;
  m_parton_t2_phi         = -99.;
  m_parton_t2_e           = -99.;
  m_parton_t2_m           = -99.;
  m_parton_t2_y           = -99.;

  m_parton_ttbar_pt        = -99.;
  m_parton_ttbar_eta       = -99.;
  m_parton_ttbar_phi       = -99.;
  m_parton_ttbar_e         = -99.;
  m_parton_ttbar_m         = -99.;
  m_parton_ttbar_y         = -99.;
    
  m_parton_b_pt            = -99.;
  m_parton_b_eta           = -99.;
  m_parton_b_phi           = -99.;
  m_parton_b_e             = -99.;
    
  m_parton_bbar_pt         = -99.;
  m_parton_bbar_eta        = -99.;
  m_parton_bbar_phi        = -99.;
  m_parton_bbar_e          = -99.;
    
  m_parton_l_pt            = -99.;
  m_parton_l_eta           = -99.;
  m_parton_l_phi           = -99.;
  m_parton_l_e             = -99.;
  m_parton_l_pdgId         = 0;
    
  m_parton_lbar_pt         = -99.;
  m_parton_lbar_eta        = -99.;
  m_parton_lbar_phi        = -99.;
  m_parton_lbar_e          = -99.;
  m_parton_lbar_pdgId      = 0;

  m_parton_nu_pt            = -99.;
  m_parton_nu_eta           = -99.;
  m_parton_nu_phi           = -99.;
  m_parton_nu_e             = -99.;
  m_parton_nu_pdgId         = 0;
    
  m_parton_nubar_pt         = -99.;
  m_parton_nubar_eta        = -99.;
  m_parton_nubar_phi        = -99.;
  m_parton_nubar_e          = -99.;
  m_parton_nubar_pdgId      = 0;

  m_parton_Wp_pt           = -99.;
  m_parton_Wp_eta          = -99.;
  m_parton_Wp_phi          = -99.;
  m_parton_Wp_e            = -99.;
    
  m_parton_Wm_pt           = -99.;
  m_parton_Wm_eta          = -99.;
  m_parton_Wm_phi          = -99.;
  m_parton_Wm_e            = -99.;
    
  m_parton_b_prime_pt      = -99.;
  m_parton_b_prime_eta     = -99.;
  m_parton_b_prime_phi     = -99.;
  m_parton_b_prime_e       = -99.;

  m_parton_bbar_prime_pt   = -99.;
  m_parton_bbar_prime_eta  = -99.;
  m_parton_bbar_prime_phi  = -99.;
  m_parton_bbar_prime_e    = -99.;

  m_parton_bbbar_prime_pt   = -99.;
  m_parton_bbbar_prime_eta  = -99.;
  m_parton_bbbar_prime_phi  = -99.;
  m_parton_bbbar_prime_e    = -99.;

  m_parton_cos_helicity_p   = -99.;
  m_parton_cos_helicity_m   = -99.;
  m_parton_cos_raxis_p      = -99.;
  m_parton_cos_raxis_m      = -99.;
  m_parton_cos_transverse_p = -99.;
  m_parton_cos_transverse_m = -99.;
  m_parton_opening_angle    = -99.;
  m_parton_phi_star_t_tbar  = -99.;

  m_parton_dilep_delta_phi = -99.;
  m_parton_dilep_delta_eta = -99.;
  
  m_parton_weight_mc       = -99.;
  m_parton_eventNumber     = -99.;
  m_parton_runNumber       = -99.;

  m_parton_top  = TLorentzVector();
  m_parton_tbar = TLorentzVector();
  m_parton_b    = TLorentzVector();
  m_parton_bbar = TLorentzVector();
  m_parton_l    = TLorentzVector();
  m_parton_lbar = TLorentzVector();
  m_parton_nu    = TLorentzVector();
  m_parton_nubar = TLorentzVector();
  m_parton_Wp   = TLorentzVector();
  m_parton_Wm   = TLorentzVector();
 
  m_parton_is_tau = false; 
}

void TopDileptonEventSaver::saveTruthEvent(){

  ///-- Reset everything --///
  initializePartonLevelEvent();

  const xAOD::EventInfo* eventInfo(nullptr);
  top::check( evtStore()->retrieve(eventInfo, m_config->sgKeyEventInfo()) , "Failed to retrieve EventInfo" );
  
  const xAOD::TruthEventContainer * truthEvent(nullptr);
  top::check( evtStore()->retrieve(truthEvent, m_config->sgKeyTruthEvent()) , "Failed to retrieve truth event container" );
  unsigned int truthEventSize = truthEvent->size();
  top::check( truthEventSize==1 , "Failed to retrieve truth PDF info - truth event container size is different from 1 (strange)" );
  
  m_parton_weight_mc   = truthEvent->at(0)->weights()[0];                                                                        
  m_parton_eventNumber = eventInfo->eventNumber();
  m_parton_runNumber   = eventInfo->runNumber();
  
  ///-- Try to get the tops after FSR --///
  const xAOD::PartonHistoryContainer* topPartonCont = nullptr;
  top::check(evtStore()->event()->retrieve(topPartonCont, "TopPartonHistory"), "FAILURE");
  const xAOD::PartonHistory *topParton = topPartonCont->at(0);
 
  ///-- tau neutrino flag --///
  if(abs(topParton->auxdata<int>("W_from_t_decay1_before_FSR_pdgId")) == 16 ||
     abs(topParton->auxdata<int>("W_from_t_decay2_before_FSR_pdgId")) == 16 ||
     abs(topParton->auxdata<int>("W_from_tbar_decay1_before_FSR_pdgId")) == 16 ||
     abs(topParton->auxdata<int>("W_from_tbar_decay2_before_FSR_pdgId")) == 16)
    m_parton_is_tau = 1;

  bool top_set  = false;
  bool tbar_set = false;
  bool b_set    = false;
  bool bbar_set = false;
  bool l_set    = false;
  bool lbar_set = false;

  if (topParton->auxdata<float>("t_after_FSR_pt")>0) {
    m_parton_top.SetPtEtaPhiM(topParton->auxdata<float>("t_after_FSR_pt"),
                              topParton->auxdata<float>("t_after_FSR_eta"),
                              topParton->auxdata<float>("t_after_FSR_phi"),
                              topParton->auxdata<float>("t_after_FSR_m"));
    top_set = true;
  }
  if (topParton->auxdata<float>("tbar_after_FSR_pt")>0) {
    m_parton_tbar.SetPtEtaPhiM(topParton->auxdata<float>("tbar_after_FSR_pt"),
                               topParton->auxdata<float>("tbar_after_FSR_eta"),
                               topParton->auxdata<float>("tbar_after_FSR_phi"),
                               topParton->auxdata<float>("tbar_after_FSR_m"));
    tbar_set = true;
  }

  if (topParton->auxdata<float>("b_from_t_before_FSR_pt")>0) {
    m_parton_b.SetPtEtaPhiM(topParton->auxdata<float>("b_from_t_before_FSR_pt"),
                            topParton->auxdata<float>("b_from_t_before_FSR_eta"),
                            topParton->auxdata<float>("b_from_t_before_FSR_phi"),
                            topParton->auxdata<float>("b_from_t_before_FSR_m"));
    b_set = true;
  }
  if (topParton->auxdata<float>("bbar_from_tbar_before_FSR_pt")>0) {
    m_parton_bbar.SetPtEtaPhiM(topParton->auxdata<float>("bbar_from_tbar_before_FSR_pt"),
                               topParton->auxdata<float>("bbar_from_tbar_before_FSR_eta"),
                               topParton->auxdata<float>("bbar_from_tbar_before_FSR_phi"),
                               topParton->auxdata<float>("bbar_from_tbar_before_FSR_m"));
    bbar_set = true;
  }

  
  ///-- W's before FSR --///
  if (topParton->auxdata<float>("W_from_t_before_FSR_pt")>0){
    m_parton_Wp.SetPtEtaPhiM(topParton->auxdata<float>("W_from_t_before_FSR_pt"),
                             topParton->auxdata<float>("W_from_t_before_FSR_eta"),
                             topParton->auxdata<float>("W_from_t_before_FSR_phi"),
                             topParton->auxdata<float>("W_from_t_before_FSR_m"));
  }

  if (topParton->auxdata<float>("W_from_tbar_before_FSR_pt")>0){
    m_parton_Wm.SetPtEtaPhiM(topParton->auxdata<float>("W_from_tbar_before_FSR_pt"),
                             topParton->auxdata<float>("W_from_tbar_before_FSR_eta"),
                             topParton->auxdata<float>("W_from_tbar_before_FSR_phi"),
                             topParton->auxdata<float>("W_from_tbar_before_FSR_m"));
  }


  ///-- leptons before FSR --///
  if (topParton->auxdata<int>("W_from_t_decay1_before_FSR_pdgId") == -11 || topParton->auxdata<int>("W_from_t_decay1_before_FSR_pdgId") == -13){
    m_parton_lbar_pdgId = topParton->auxdata<int>("W_from_t_decay1_before_FSR_pdgId");
    m_parton_lbar.SetPtEtaPhiM(topParton->auxdata<float>("W_from_t_decay1_before_FSR_pt"),
                               topParton->auxdata<float>("W_from_t_decay1_before_FSR_eta"),
                               topParton->auxdata<float>("W_from_t_decay1_before_FSR_phi"),
                               topParton->auxdata<float>("W_from_t_decay1_before_FSR_m"));
    lbar_set = true;
  }
  if (topParton->auxdata<int>("W_from_t_decay2_before_FSR_pdgId") == -11 || topParton->auxdata<int>("W_from_t_decay2_before_FSR_pdgId") == -13){
    m_parton_lbar_pdgId = topParton->auxdata<int>("W_from_t_decay2_before_FSR_pdgId");
    m_parton_lbar.SetPtEtaPhiM(topParton->auxdata<float>("W_from_t_decay2_before_FSR_pt"),
                               topParton->auxdata<float>("W_from_t_decay2_before_FSR_eta"),
                               topParton->auxdata<float>("W_from_t_decay2_before_FSR_phi"),
                               topParton->auxdata<float>("W_from_t_decay2_before_FSR_m"));
    lbar_set = true;
  }

  if (topParton->auxdata<int>("W_from_tbar_decay1_before_FSR_pdgId") == 11 || topParton->auxdata<int>("W_from_tbar_decay1_before_FSR_pdgId") == 13){
    m_parton_l_pdgId = topParton->auxdata<int>("W_from_tbar_decay1_before_FSR_pdgId");
    m_parton_l.SetPtEtaPhiM(topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_pt"),
                            topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_eta"),
                            topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_phi"),
                            topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_m"));
    l_set = true;
  }
  if (topParton->auxdata<int>("W_from_tbar_decay2_before_FSR_pdgId") == 11 || topParton->auxdata<int>("W_from_tbar_decay2_before_FSR_pdgId") == 13){
    m_parton_l_pdgId = topParton->auxdata<int>("W_from_tbar_decay2_before_FSR_pdgId");
    m_parton_l.SetPtEtaPhiM(topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_pt"),
                            topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_eta"),
                            topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_phi"),
                            topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_m"));
    l_set = true;
  }


  ///-- Neutrinos before FSR --///
  if (topParton->auxdata<int>("W_from_t_decay1_before_FSR_pdgId") == 12 || topParton->auxdata<int>("W_from_t_decay1_before_FSR_pdgId") == 14){
    m_parton_nu_pdgId = topParton->auxdata<int>("W_from_t_decay1_before_FSR_pdgId");
    m_parton_nu.SetPtEtaPhiM(topParton->auxdata<float>("W_from_t_decay1_before_FSR_pt"),
                             topParton->auxdata<float>("W_from_t_decay1_before_FSR_eta"),
                             topParton->auxdata<float>("W_from_t_decay1_before_FSR_phi"),
                             topParton->auxdata<float>("W_from_t_decay1_before_FSR_m"));
  }
  if (topParton->auxdata<int>("W_from_t_decay2_before_FSR_pdgId") == 12 || topParton->auxdata<int>("W_from_t_decay2_before_FSR_pdgId") == 14){
    m_parton_nu_pdgId = topParton->auxdata<int>("W_from_t_decay2_before_FSR_pdgId");
    m_parton_nu.SetPtEtaPhiM(topParton->auxdata<float>("W_from_t_decay2_before_FSR_pt"),
                             topParton->auxdata<float>("W_from_t_decay2_before_FSR_eta"),
                             topParton->auxdata<float>("W_from_t_decay2_before_FSR_phi"),
                             topParton->auxdata<float>("W_from_t_decay2_before_FSR_m"));
  }

  // tbar->W- + bbar; W- ->l + nubar => nubar from tbar
  if (topParton->auxdata<int>("W_from_tbar_decay1_before_FSR_pdgId") == -12 || topParton->auxdata<int>("W_from_tbar_decay1_before_FSR_pdgId") == -14){
    m_parton_nubar_pdgId = topParton->auxdata<int>("W_from_tbar_decay1_before_FSR_pdgId");
    m_parton_nubar.SetPtEtaPhiM(topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_pt"),
                                topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_eta"),
                                topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_phi"),
                                topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_m"));
  }
  if (topParton->auxdata<int>("W_from_tbar_decay2_before_FSR_pdgId") == -12 || topParton->auxdata<int>("W_from_tbar_decay2_before_FSR_pdgId") == -14){
    m_parton_nubar_pdgId = topParton->auxdata<int>("W_from_tbar_decay2_before_FSR_pdgId");
    m_parton_nubar.SetPtEtaPhiM(topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_pt"),
                                topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_eta"),
                                topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_phi"),
                                topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_m"));
  }

  /*
  if (topParton->auxdata<int>("W_from_t_decay1_before_FSR_pdgId") == -12 || topParton->auxdata<int>("W_from_t_decay1_before_FSR_pdgId") == -14){
    m_parton_nubar_pdgId = topParton->auxdata<int>("W_from_t_decay1_before_FSR_pdgId");
    m_parton_nubar.SetPtEtaPhiM(topParton->auxdata<float>("W_from_t_decay1_before_FSR_pt"),
				topParton->auxdata<float>("W_from_t_decay1_before_FSR_eta"),
				topParton->auxdata<float>("W_from_t_decay1_before_FSR_phi"),
				topParton->auxdata<float>("W_from_t_decay1_before_FSR_m"));
  }
  if (topParton->auxdata<int>("W_from_t_decay2_before_FSR_pdgId") == -12 || topParton->auxdata<int>("W_from_t_decay2_before_FSR_pdgId") == -14){
    m_parton_nubar_pdgId = topParton->auxdata<int>("W_from_t_decay2_before_FSR_pdgId");
    m_parton_nubar.SetPtEtaPhiM(topParton->auxdata<float>("W_from_t_decay2_before_FSR_pt"),
				topParton->auxdata<float>("W_from_t_decay2_before_FSR_eta"),
				topParton->auxdata<float>("W_from_t_decay2_before_FSR_phi"),
				topParton->auxdata<float>("W_from_t_decay2_before_FSR_m"));
  }

  if (topParton->auxdata<int>("W_from_tbar_decay1_before_FSR_pdgId") == 12 || topParton->auxdata<int>("W_from_tbar_decay1_before_FSR_pdgId") == 14){
    m_parton_nu_pdgId = topParton->auxdata<int>("W_from_tbar_decay1_before_FSR_pdgId");
    m_parton_nu.SetPtEtaPhiM(topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_pt"),
			     topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_eta"),
			     topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_phi"),
			     topParton->auxdata<float>("W_from_tbar_decay1_before_FSR_m"));
  }
  if (topParton->auxdata<int>("W_from_tbar_decay2_before_FSR_pdgId") == 12 || topParton->auxdata<int>("W_from_tbar_decay2_before_FSR_pdgId") == 14){
    m_parton_nu_pdgId = topParton->auxdata<int>("W_from_tbar_decay2_before_FSR_pdgId");
    m_parton_nu.SetPtEtaPhiM(topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_pt"),
			     topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_eta"),
			     topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_phi"),
			     topParton->auxdata<float>("W_from_tbar_decay2_before_FSR_m"));
  }
  */

  ///-- top after FSR --///
  if (top_set && tbar_set && b_set && bbar_set && l_set && lbar_set){
    TLorentzVector m_truth_ttbar = m_parton_top + m_parton_tbar;
  
    m_parton_top_pt     = m_parton_top.Pt()/1000.;
    m_parton_top_eta    = m_parton_top.Eta();
    m_parton_top_phi    = m_parton_top.Phi();
    m_parton_top_e      = m_parton_top.E()/1000.;
    m_parton_top_m      = m_parton_top.M()/1000.;
    m_parton_top_y      = m_parton_top.Rapidity();
    
    m_parton_tbar_pt    = m_parton_tbar.Pt()/1000.;
    m_parton_tbar_eta   = m_parton_tbar.Eta();
    m_parton_tbar_phi   = m_parton_tbar.Phi();
    m_parton_tbar_e     = m_parton_tbar.E()/1000.;
    m_parton_tbar_m     = m_parton_tbar.M()/1000.;
    m_parton_tbar_y     = m_parton_tbar.Rapidity();

    m_parton_av_top_pt  = (m_parton_top_pt  + m_parton_tbar_pt)/2.;
    m_parton_av_top_eta = (m_parton_top_eta + m_parton_tbar_eta)/2.;
    m_parton_av_top_phi = (m_parton_top_phi + m_parton_tbar_phi)/2.;
    m_parton_av_top_e   = (m_parton_top_e   + m_parton_tbar_e)/2.;
    m_parton_av_top_m   = (m_parton_top_m   + m_parton_tbar_m)/2.;
    m_parton_av_top_y   = (m_parton_top_y   + m_parton_tbar_y)/2.;
    
    if (m_parton_top_pt > m_parton_tbar_pt){
      m_parton_t1_pt       = m_parton_top_pt;
      m_parton_t1_eta      = m_parton_top_eta;
      m_parton_t1_phi      = m_parton_top_phi;
      m_parton_t1_m        = m_parton_top_m;
      m_parton_t1_e        = m_parton_top_e;
      m_parton_t1_y        = m_parton_top_y;
      m_parton_t2_pt       = m_parton_tbar_pt;
      m_parton_t2_eta      = m_parton_tbar_eta;
      m_parton_t2_phi      = m_parton_tbar_phi;
      m_parton_t2_m        = m_parton_tbar_m;
      m_parton_t2_e        = m_parton_tbar_e;
      m_parton_t2_y        = m_parton_tbar_y;
    } else {
      m_parton_t2_pt       = m_parton_top_pt;
      m_parton_t2_eta      = m_parton_top_eta;
      m_parton_t2_phi      = m_parton_top_phi;
      m_parton_t2_m        = m_parton_top_m;
      m_parton_t2_e        = m_parton_top_e;
      m_parton_t2_y        = m_parton_top_y;
      m_parton_t1_pt       = m_parton_tbar_pt;
      m_parton_t1_eta      = m_parton_tbar_eta;
      m_parton_t1_phi      = m_parton_tbar_phi;
      m_parton_t1_m        = m_parton_tbar_m;
      m_parton_t1_e        = m_parton_tbar_e;
      m_parton_t1_y        = m_parton_tbar_y;
    }
    
    m_parton_ttbar_pt  = m_truth_ttbar.Pt()/1000.;
    m_parton_ttbar_eta = m_truth_ttbar.Eta();
    m_parton_ttbar_phi = m_truth_ttbar.Phi();
    m_parton_ttbar_m   = m_truth_ttbar.M()/1000.;
    m_parton_ttbar_e   = m_truth_ttbar.E()/1000.;
    m_parton_ttbar_y   = m_truth_ttbar.Rapidity();
    
    m_parton_b_pt      = m_parton_b.Pt()/1000.;
    m_parton_b_eta     = m_parton_b.Eta();
    m_parton_b_phi     = m_parton_b.Phi();
    m_parton_b_e       = m_parton_b.E()/1000.;
    
    m_parton_bbar_pt   = m_parton_bbar.Pt()/1000.;
    m_parton_bbar_eta  = m_parton_bbar.Eta();
    m_parton_bbar_phi  = m_parton_bbar.Phi();
    m_parton_bbar_e    = m_parton_bbar.E()/1000.;
    
    m_parton_l_pt      = m_parton_l.Pt()/1000.;
    m_parton_l_eta     = m_parton_l.Eta();
    m_parton_l_phi     = m_parton_l.Phi();
    m_parton_l_e       = m_parton_l.E()/1000.;
    
    m_parton_lbar_pt   = m_parton_lbar.Pt()/1000.;
    m_parton_lbar_eta  = m_parton_lbar.Eta();
    m_parton_lbar_phi  = m_parton_lbar.Phi();
    m_parton_lbar_e    = m_parton_lbar.E()/1000.;

    m_parton_nu_pt      = m_parton_nu.Pt()/1000.;
    m_parton_nu_eta     = m_parton_nu.Eta();
    m_parton_nu_phi     = m_parton_nu.Phi();
    m_parton_nu_e       = m_parton_nu.E()/1000.;
    
    m_parton_nubar_pt   = m_parton_nubar.Pt()/1000.;
    m_parton_nubar_eta  = m_parton_nubar.Eta();
    m_parton_nubar_phi  = m_parton_nubar.Phi();
    m_parton_nubar_e    = m_parton_nubar.E()/1000.;
    
    m_parton_ttbar_dphi = calculateDphi_ttbar( m_parton_top, m_parton_tbar );
    m_parton_ttbar_pout = calculatePout( m_parton_top, m_parton_tbar )/1000.;
    
    m_parton_cos_helicity_p   = cos_theta_helicity(   m_parton_top,  m_parton_top,  m_truth_ttbar, m_parton_lbar, +1);
    m_parton_cos_helicity_m   = cos_theta_helicity(   m_parton_top,  m_parton_tbar, m_truth_ttbar, m_parton_l,    -1);
    m_parton_cos_raxis_p      = cos_theta_raxis(      m_parton_top,  m_parton_top,  m_truth_ttbar, m_parton_lbar, +1);
    m_parton_cos_raxis_m      = cos_theta_raxis(      m_parton_top,  m_parton_tbar, m_truth_ttbar, m_parton_l,    -1);
    m_parton_cos_transverse_p = cos_theta_transverse( m_parton_top,  m_parton_top,  m_truth_ttbar, m_parton_lbar, +1);
    m_parton_cos_transverse_m = cos_theta_transverse( m_parton_top,  m_parton_tbar, m_truth_ttbar, m_parton_l,    -1);
    m_parton_opening_angle    = opening_angle(        m_truth_ttbar, m_parton_l, m_parton_lbar);
    m_parton_phi_star_t_tbar  = phi_star(             m_parton_top,  m_parton_tbar);


    
    m_parton_dilep_delta_phi = m_parton_l.DeltaPhi(m_parton_lbar);
    m_parton_dilep_delta_eta = fabs(m_parton_l.Eta() - m_parton_lbar.Eta());
  


    //m_truthTreeManager->fill();
  }
    ///-- only write out info if objects pass fiducial requirements --///

  if( (abs(m_parton_l_pdgId) == 11 && abs(m_parton_lbar_pdgId) == 13) ||
      (abs(m_parton_l_pdgId) == 13 && abs(m_parton_lbar_pdgId) == 11)){
    EventSaverFlatNtuple::saveTruthEvent();

  }
}

void TopDileptonEventSaver::saveParticleLevelEvent(const top::ParticleLevelEvent& plEvent){
  
  //if ( not particleLevelTreeManager() ){
    //setupParticleLevelTreeManager(plEvent);
  //}
  
  initializeParticleLevelBranches();
  
  if (!is_MC )
    return;

  m_particle_eventNumber = plEvent.m_info->eventNumber();

  TLorentzVector lep_pos, lep_neg, nu, nubar, b, bbar, top, tbar, jet1, jet2, dilep;

  //////////////////////////////////
  ///--    Particle leptons    --///
  //////////////////////////////////

  ///-- electrons --///
  for (const auto & elPtr : * plEvent.m_electrons) {
    m_particle_lep_type.push_back(11);
    m_particle_lep_pt.push_back(elPtr->pt()/1000.);
    m_particle_lep_eta.push_back(elPtr->eta());
    m_particle_lep_phi.push_back(elPtr->phi());
    m_particle_lep_e.push_back(elPtr->e()/1000.);
    m_particle_lep_charge.push_back(elPtr->charge());
    ++m_particle_lep_n;
  }
  
  ///-- muons --///
  for (const auto & muPtr : * plEvent.m_muons) {
    m_particle_lep_type.push_back(13);
    m_particle_lep_pt.push_back(muPtr->pt()/1000.);
    m_particle_lep_eta.push_back(muPtr->eta());
    m_particle_lep_phi.push_back(muPtr->phi());
    m_particle_lep_e.push_back(muPtr->e()/1000.);
    m_particle_lep_charge.push_back(muPtr->charge());
    ++m_particle_lep_n;
  }

  if (m_particle_lep_n == 2){
    if (m_particle_lep_charge[0] > 0 && m_particle_lep_charge[1] < 0){
      lep_pos.SetPtEtaPhiE(m_particle_lep_pt[0], m_particle_lep_eta[0], m_particle_lep_phi[0], m_particle_lep_e[0]);
      lep_neg.SetPtEtaPhiE(m_particle_lep_pt[1], m_particle_lep_eta[1], m_particle_lep_phi[1], m_particle_lep_e[1]);
    } else if (m_particle_lep_charge[0] < 0 && m_particle_lep_charge[1] > 0){
      lep_pos.SetPtEtaPhiE(m_particle_lep_pt[1], m_particle_lep_eta[1], m_particle_lep_phi[1], m_particle_lep_e[1]);
      lep_neg.SetPtEtaPhiE(m_particle_lep_pt[0], m_particle_lep_eta[0], m_particle_lep_phi[0], m_particle_lep_e[0]);
    } else {
      std::cout << "WARNING: Particle same-sign lepton event!" << std::endl;
    }
   
    dilep = lep_pos + lep_neg;
    m_particle_dilep_m         = dilep.M();
    m_particle_dilep_pt        = dilep.Pt();
    m_particle_dilep_delta_phi = lep_pos.DeltaPhi(lep_neg);
    m_particle_dilep_delta_eta = fabs(lep_pos.Eta() - lep_neg.Eta());
  }

  ///////////////////////////////
  ///--    Particle jets    --///
  ///////////////////////////////

  for (const auto & jetPtr : * plEvent.m_jets) {
    m_particle_jet_pt.push_back(jetPtr->pt()/1000.);
    m_particle_jet_eta.push_back(jetPtr->eta());
    m_particle_jet_phi.push_back(jetPtr->phi());
    m_particle_jet_e.push_back(jetPtr->e()/1000.);
    m_particle_jet_ghosts.push_back(jetPtr->auxdata<int>( "GhostBHadronsFinalCount" ));
    ++m_particle_jet_n;
    if( jetPtr->auxdata<int>( "GhostBHadronsFinalCount" ) > 0){
      m_particle_bjet_pt.push_back(jetPtr->pt()/1000.);
      m_particle_bjet_eta.push_back(jetPtr->eta());
      m_particle_bjet_phi.push_back(jetPtr->phi());
      m_particle_bjet_e.push_back(jetPtr->e()/1000.);
      m_particle_bjet_ghosts.push_back(jetPtr->auxdata<int>( "GhostBHadronsFinalCount" ));
      ++m_particle_bjet_n;
    }
  }

  ////////////////////////////////////
  ///--    Particle Neutrinos    --///
  ///////////////////////////////////

  const xAOD::TruthParticleContainer* neutrinos{nullptr};
  top::check(evtStore()->retrieve(neutrinos, "TruthNeutrinos"), "Failed to retrieve Truth Neutrinos");

  ///-- Here we rely on the ordering of the Neutrinos, i.e. that the first two are the hard ones and the subsequent ones come from further tau decay or from jets --///

  bool nu_set     = false;
  bool nubar_set  = false;
  int nu_pdgId    = 0;
  int nubar_pdgId = 0;
  int number_of_tau_neutrinos = 0;
  if (neutrinos != nullptr) {
    for (const auto& neutrino : *neutrinos) {
      ++m_particle_nu_n;  
      if( abs(neutrino->pdgId()) == 16) ++number_of_tau_neutrinos;
      if(!nu_set || !nubar_set){
	if( neutrino->pdgId() > 0 && !nu_set){
	  nu.SetPtEtaPhiE(neutrino->pt()/1000., neutrino->eta(), neutrino->phi(), neutrino->e()/1000.);
	  m_particle_nu_pt     = nu.Pt();
	  m_particle_nu_eta    = nu.Eta();
	  m_particle_nu_phi    = nu.Phi();
	  m_particle_nu_e      = nu.E();
	  m_particle_nu_m      = nu.M();
	  m_particle_nu_y      = nu.Rapidity();
	  nu_set = true;
	  nu_pdgId = neutrino->pdgId();
	}
	if( neutrino->pdgId() < 0 && !nubar_set){
	  nubar.SetPtEtaPhiE(neutrino->pt()/1000., neutrino->eta(), neutrino->phi(), neutrino->e()/1000.);
	  m_particle_nubar_pt  = nubar.Pt();
	  m_particle_nubar_eta = nubar.Eta();
	  m_particle_nubar_phi = nubar.Phi();
	  m_particle_nubar_e   = nubar.E();
	  m_particle_nubar_m   = nubar.M();
	  m_particle_nubar_y   = nubar.Rapidity();

	  nubar_set = true;
	  nubar_pdgId = neutrino->pdgId();
	}
      }
    }
  }

  m_particle_is_tau = m_parton_is_tau;
    
  ///-- Met --///
  m_particle_met_et  = plEvent.m_met->met()/1000.;
  m_particle_met_phi = plEvent.m_met->phi();
  m_particle_met_ex  = plEvent.m_met->mpx()/1000.;
  m_particle_met_ey  = plEvent.m_met->mpy()/1000.;
  
  //////////////////////////////////////////
  ///--   Event Selection and Top Reco --///
  //////////////////////////////////////////
  
  if (m_particle_lep_n == 2 && 
      ((abs(m_particle_lep_type[0]) == 11 && abs(m_particle_lep_type[1]) == 13) ||
       (abs(m_particle_lep_type[1]) == 11 && abs(m_particle_lep_type[0]) == 13)) && 
      ((m_particle_lep_pt[0] >= 27. && m_particle_lep_pt[1] >= 25.) ||
       (m_particle_lep_pt[0] >= 25. && m_particle_lep_pt[0] >= 27.)) &&
      abs(m_particle_lep_eta[0]) <= 2.5 && abs(m_particle_lep_eta[1]) <= 2.5 && 
      (m_particle_lep_charge[0] * m_particle_lep_charge[1] < 0)){
    m_particle_pass_emu = true;
    
    ///-- Pseudo Top Reconstruction--///
    if(m_particle_bjet_n > 0 && m_particle_jet_n > 1){
      
      if(m_particle_bjet_n > 1){
	jet1.SetPtEtaPhiE(m_particle_bjet_pt[0], m_particle_bjet_eta[0], m_particle_bjet_phi[0], m_particle_bjet_e[0]);
	jet2.SetPtEtaPhiE(m_particle_bjet_pt[1], m_particle_bjet_eta[1], m_particle_bjet_phi[1], m_particle_bjet_e[1]);
      } else {
	jet1.SetPtEtaPhiE(m_particle_bjet_pt[0], m_particle_bjet_eta[0], m_particle_bjet_phi[0], m_particle_bjet_e[0]);
	if (m_particle_jet_pt[0] != m_particle_bjet_pt[0]){
	  jet2.SetPtEtaPhiE(m_particle_jet_pt[0], m_particle_jet_eta[0], m_particle_jet_phi[0], m_particle_jet_e[0]);
	} else {
	  jet2.SetPtEtaPhiE(m_particle_jet_pt[1], m_particle_jet_eta[1], m_particle_jet_phi[1], m_particle_jet_e[1]);
	}
      } 
      
      TLorentzVector Wp, Wm, top_a, tbar_a, top_b, tbar_b;
      
      Wp = lep_pos + nu;
      Wm = lep_neg + nubar;
      
      top_a  = Wp + jet1;
      top_b  = Wp + jet2;
      
      tbar_a = Wm + jet2;
      tbar_b = Wm + jet1;
      
      double diff_a = fabs(top_a.M() - 172.5) + fabs(tbar_a.M() - 172.5);
      double diff_b = fabs(top_b.M() - 172.5) + fabs(tbar_b.M() - 172.5);
      if(diff_a < diff_b){ // A is the correct combination
        top  = top_a;
	tbar = tbar_a;
	m_particle_b_index    = 0;
	m_particle_bbar_index = 1;	
      } else if (diff_a > diff_b){ // B is the right combination
	top  = top_b;
      	tbar = tbar_b;
      	m_particle_b_index    = 1;
      	m_particle_bbar_index = 0;		
      } else {
      	std::cout << "What the fuck? Top Diff a = " << diff_a << " diff b = " << diff_b << std::endl;
      	top  = top_a;
      	tbar = tbar_a;
      	m_particle_b_index = -1.;
      	m_particle_bbar_index = -1.;		
      }
      
      TLorentzVector ttbar = top + tbar;
      
      m_particle_Wp_pt        = Wp.Pt();
      m_particle_Wp_eta       = Wp.Eta();
      m_particle_Wp_phi       = Wp.Phi();
      m_particle_Wp_m         = Wp.M();
      m_particle_Wp_e         = Wp.E();
      m_particle_Wp_y         = Wp.Rapidity();
      m_particle_Wm_pt        = Wm.Pt();
      m_particle_Wm_eta       = Wm.Eta();
      m_particle_Wm_phi       = Wm.Phi();
      m_particle_Wm_m         = Wm.M();
      m_particle_Wm_e         = Wm.E();
      m_particle_Wm_y         = Wm.Rapidity();

      m_particle_top_pt       = top.Pt();
      m_particle_top_eta      = top.Eta();
      m_particle_top_phi      = top.Phi();
      m_particle_top_m        = top.M();
      m_particle_top_e        = top.E();
      m_particle_top_y        = top.Rapidity();

      m_particle_tbar_pt      = tbar.Pt();
      m_particle_tbar_eta     = tbar.Eta();
      m_particle_tbar_phi     = tbar.Phi();
      m_particle_tbar_m       = tbar.M();
      m_particle_tbar_e       = tbar.E();
      m_particle_tbar_y       = tbar.Rapidity();

      m_particle_av_top_pt    = (m_particle_top_pt  + m_particle_tbar_pt)/2.;
      m_particle_av_top_eta   = (m_particle_top_eta + m_particle_tbar_eta)/2.;
      m_particle_av_top_phi   = (m_particle_top_phi + m_particle_tbar_phi)/2.;
      m_particle_av_top_m     = (m_particle_top_m   + m_particle_tbar_m)/2.;
      m_particle_av_top_e     = (m_particle_top_e   + m_particle_tbar_e)/2.;
      m_particle_av_top_y     = (m_particle_top_y   + m_particle_tbar_y)/2.;

      if (top.Pt() > tbar.Pt()){
	m_particle_t1_pt       = top.Pt();
	m_particle_t1_eta      = top.Eta();
	m_particle_t1_phi      = top.Phi();
	m_particle_t1_m        = top.M();
	m_particle_t1_e        = top.E();
	m_particle_t1_y        = top.Rapidity();
	m_particle_t2_pt       = tbar.Pt();
	m_particle_t2_eta      = tbar.Eta();
	m_particle_t2_phi      = tbar.Phi();
	m_particle_t2_m        = tbar.M();
	m_particle_t2_e        = tbar.E();
	m_particle_t2_y        = tbar.Rapidity();
      } else {
	m_particle_t2_pt       = top.Pt();
	m_particle_t2_eta      = top.Eta();
	m_particle_t2_phi      = top.Phi();
	m_particle_t2_m        = top.M();
	m_particle_t2_e        = top.E();
	m_particle_t2_y        = top.Rapidity();
	m_particle_t1_pt       = tbar.Pt();
	m_particle_t1_eta      = tbar.Eta();
	m_particle_t1_phi      = tbar.Phi();
	m_particle_t1_m        = tbar.M();
	m_particle_t1_e        = tbar.E();
	m_particle_t1_y        = tbar.Rapidity();
      }


      m_particle_ttbar_pt     = ttbar.Pt();
      m_particle_ttbar_eta    = ttbar.Eta();
      m_particle_ttbar_phi    = ttbar.Phi();
      m_particle_ttbar_m      = ttbar.M();
      m_particle_ttbar_e      = ttbar.E();
      m_particle_ttbar_y      = ttbar.Rapidity();
      m_particle_ttbar_dphi   = calculateDphi_ttbar( m_parton_top, m_parton_tbar );
      m_particle_ttbar_pout   = calculatePout( top, tbar );

      m_particle_cos_helicity_p   = cos_theta_helicity(   top,  top,  ttbar, lep_pos, +1);
      m_particle_cos_helicity_m   = cos_theta_helicity(   top,  tbar, ttbar, lep_neg, -1);
      m_particle_cos_raxis_p      = cos_theta_raxis(      top,  top,  ttbar, lep_pos, +1);
      m_particle_cos_raxis_m      = cos_theta_raxis(      top,  tbar, ttbar, lep_neg, -1);
      m_particle_cos_transverse_p = cos_theta_transverse( top,  top,  ttbar, lep_pos, +1);
      m_particle_cos_transverse_m = cos_theta_transverse( top,  tbar, ttbar, lep_neg, -1);
      m_particle_opening_angle    = opening_angle(        ttbar, lep_neg, lep_pos);
      m_particle_phi_star_t_tbar  = phi_star(             top, tbar);  




    }
  }
  top::EventSaverFlatNtuple::saveParticleLevelEvent(plEvent);
  
}


void TopDileptonEventSaver::initializeRecoLevelBranches(){
 
  m_jet_from_top_index = -99;
  m_jet_from_tbar_index = -99; 

  m_non_top_mass = -99;
 
  ///-- Lepton Parameters --///
  m_pass_emu          = false;
  m_pass_emu_pretag   = false;
  m_pass_control_emu  = false;
  m_fakeEvent         = false;
  m_reco_is_tau = false;

  m_lep_n             = 0;
  m_el_n              = 0;
  m_mu_n              = 0;
  
  m_mlb.clear();  
  m_dilep_m           = -1;
  m_dilep_pt          = -1;
  m_dilep_delta_phi   = -1;
  m_dilep_delta_eta   = -1;
  
  m_met_et            = -1;
  m_met_phi           = -1;
  m_met_ex            = -1;
  m_met_ey            = -1;
  m_met_sumet         = -1;
  
  jet_n               = 0;
  m_jet_n             = 0;
  m_bjet_n            = 0;
  m_bjet_n_85         = 0;
  m_bjet_n_77         = 0;
  m_bjet_n_70         = 0;
  m_bjet_n_60         = 0;
  
  m_weight_max        = -99;
  m2_weight_max       = -99;

  m_top_pt            = -99;
  m_top_eta           = -99;
  m_top_phi           = -99;
  m_top_e             = -99;
  m_top_m             = -99;
  m_top_y             = -99;
   
  m_tbar_pt           = -99;
  m_tbar_eta          = -99;
  m_tbar_phi          = -99;
  m_tbar_e            = -99;
  m_tbar_m            = -99;
  m_tbar_y            = -99;

  m_av_top_pt            = -99;
  m_av_top_eta           = -99;
  m_av_top_phi           = -99;
  m_av_top_e             = -99;
  m_av_top_m             = -99;
  m_av_top_y             = -99;
  
  m_t1_pt            = -99;
  m_t1_eta           = -99;
  m_t1_phi           = -99;
  m_t1_e             = -99;
  m_t1_m             = -99;
  m_t1_y             = -99;

  m_t2_pt            = -99;
  m_t2_eta           = -99;
  m_t2_phi           = -99;
  m_t2_e             = -99;
  m_t2_m             = -99;
  m_t2_y             = -99;

  m_ttbar_pt          = -99;
  m_ttbar_eta         = -99;
  m_ttbar_phi         = -99;
  m_ttbar_e           = -99;
  m_ttbar_m           = -99;
  m_ttbar_y           = -99;

  m_reco_cos_helicity_p   = -99.;
  m_reco_cos_helicity_m   = -99.;
  m_reco_cos_raxis_p      = -99.;
  m_reco_cos_raxis_m      = -99.;
  m_reco_cos_transverse_p = -99.;
  m_reco_cos_transverse_m = -99.;
  m_reco_opening_angle    = -99.;
  m_reco_phi_star_t_tbar  = -99.;
 
  m_ttbar_dphi = -99.;
  m_ttbar_pout = -99.;


  m_b_prime_pt              = -99;
  m_b_prime_px              = -99;
  m_b_prime_py              = -99;
  m_b_prime_pz              = -99;
  m_b_prime_eta             = -99;
  m_b_prime_phi             = -99;
  m_b_prime_e               = -99;
  m_b_prime_m               = -99;
  m_b_prime_y               = -99;

  m_bbar_prime_pt           = -99;
  m_bbar_prime_px           = -99;
  m_bbar_prime_py           = -99;
  m_bbar_prime_pz           = -99;
  m_bbar_prime_eta          = -99;
  m_bbar_prime_phi          = -99;
  m_bbar_prime_e            = -99;
  m_bbar_prime_m            = -99;
  m_bbar_prime_y            = -99;


  m_bbbar_prime_pt    = -99;
  m_bbbar_prime_eta   = -99;
  m_bbbar_prime_phi   = -99;
  m_bbbar_prime_m     = -99;
  
  m_bbbar_prime_delta_pt  = -99;
  m_bbbar_prime_delta_eta = -99;
  m_bbbar_prime_delta_phi = -99;
  m_bbbar_prime_dR        = -99;
  m_bbbar_prime_dy        = -99;
  m_bbbar_prime_dabsy     = -99;

  m_b_prime_index= -99;
  m_bbar_prime_index= -99;
  m_b_index= -99;
  m_bbar_index= -99;

  
  m_weight_all.clear();

  m2_top_pt            = -99;
  m2_top_eta           = -99;
  m2_top_phi           = -99;
  m2_top_e             = -99;
  m2_top_m             = -99;
  m2_top_y             = -99;
   
  m2_tbar_pt           = -99;
  m2_tbar_eta          = -99;
  m2_tbar_phi          = -99;
  m2_tbar_e            = -99;
  m2_tbar_m            = -99;
  m2_tbar_y            = -99;
  
  m2_ttbar_pt          = -99;
  m2_ttbar_eta         = -99;
  m2_ttbar_phi         = -99;
  m2_ttbar_e           = -99;
  m2_ttbar_m           = -99;
  m2_ttbar_y           = -99;
  m2_ttbar_coscos      = -99;  

  m2_b_prime_pt              = -99;
  m2_b_prime_px              = -99;
  m2_b_prime_py              = -99;
  m2_b_prime_pz              = -99;
  m2_b_prime_eta             = -99;
  m2_b_prime_phi             = -99;
  m2_b_prime_e               = -99;
  m2_b_prime_m               = -99;
  m2_b_prime_y               = -99;

  m2_bbar_prime_pt           = -99;
  m2_bbar_prime_px           = -99;
  m2_bbar_prime_py           = -99;
  m2_bbar_prime_pz           = -99;
  m2_bbar_prime_eta          = -99;
  m2_bbar_prime_phi          = -99;
  m2_bbar_prime_e            = -99;
  m2_bbar_prime_m            = -99;
  m2_bbar_prime_y            = -99;






  m2_bbbar_prime_pt    = -99;
  m2_bbbar_prime_eta   = -99;
  m2_bbbar_prime_phi   = -99;
  m2_bbbar_prime_m     = -99;
  
  m2_bbbar_prime_delta_pt  = -99;
  m2_bbbar_prime_delta_eta = -99;
  m2_bbbar_prime_delta_phi = -99;
  m2_bbbar_prime_dR        = -99;
  m2_bbbar_prime_dy        = -99;
  m2_bbbar_prime_dabsy     = -99;

  m2_b_prime_index= -99;
  m2_bbar_prime_index= -99;
  m2_b_index= -99;
  m2_bbar_index= -99;
  
  m2_weight_all.clear();

  m_nu_pt             = -99;
  m_nu_eta            = -99;
  m_nu_phi            = -99;
  m_nu_e              = -99;
  m_nu_m              = -99;
  m_nu_y              = -99;

  m_nubar_pt          = -99;
  m_nubar_eta         = -99;
  m_nubar_phi         = -99;
  m_nubar_e           = -99;
  m_nubar_m           = -99;
  m_nubar_y           = -99;

  m_Wp_pt             = -99;
  m_Wp_eta            = -99;
  m_Wp_phi            = -99;
  m_Wp_e              = -99;
  m_Wp_m              = -99;
  m_Wp_y              = -99;
      
  m_Wm_pt             = -99;
  m_Wm_eta            = -99;
  m_Wm_phi            = -99;
  m_Wm_e              = -99;
  m_Wm_m              = -99;
  m_Wm_y              = -99;
      
  m_b_pt              = -99;
  m_b_eta             = -99;
  m_b_phi             = -99;
  m_b_e               = -99;
  m_b_m               = -99;
  m_b_y               = -99;

  m_bbar_pt           = -99;
  m_bbar_eta          = -99;
  m_bbar_phi          = -99;
  m_bbar_e            = -99;
  m_bbar_m            = -99;
  m_bbar_y            = -99;



  m_jet_pt.clear();
  m_jet_eta.clear();
  m_jet_phi.clear();
  m_jet_e.clear();
  m_jet_jvt.clear();
  m_jet_jvc.clear();
  m_jet_truth.clear();

  m_bjet_pt.clear();
  m_bjet_eta.clear();
  m_bjet_phi.clear();
  m_bjet_e.clear();
  m_bjet_jvt.clear();
  m_bjet_jvc.clear();

  m_lep_pt.clear();
  m_lep_eta.clear();
  m_lep_phi.clear();
  m_lep_e.clear();
  m_lep_type.clear();
  m_lep_charge.clear();
  m_lep_iso_gradient.clear();
  m_lep_iso_gradient_loose.clear();
  m_lep_id_medium.clear();
  m_lep_id_tight.clear(); 
 
  m_lep_truth_type.clear();
}

void TopDileptonEventSaver::initializeParticleLevelBranches(){

  m_particle_lep_n           = 0;
  m_particle_lep_pt.clear();
  m_particle_lep_eta.clear();
  m_particle_lep_phi.clear();
  m_particle_lep_e.clear();
  m_particle_lep_charge.clear();
  m_particle_lep_type.clear();

  m_particle_pass_emu = false;

  m_particle_dilep_m         = -1;
  m_particle_dilep_pt        = -1;
  m_particle_dilep_delta_phi = -1;
  m_particle_dilep_delta_eta = -1;

  m_particle_met_et          = -1;
  m_particle_met_phi         = -1;
  m_particle_met_ex          = -1;
  m_particle_met_ey          = -1;

  m_particle_jet_n             = 0;
  m_particle_jet_pt.clear();
  m_particle_jet_eta.clear();
  m_particle_jet_phi.clear();
  m_particle_jet_e.clear();
  m_particle_jet_ghosts.clear();

  m_particle_bjet_n          = 0;
  m_particle_bjet_pt.clear();
  m_particle_bjet_eta.clear();
  m_particle_bjet_phi.clear();
  m_particle_bjet_e.clear();
  m_particle_bjet_ghosts.clear();
  m_particle_b_index = -99.;
  m_particle_bbar_index = -99.;  
  
  m_particle_eventNumber = -1;
  m_particle_is_tau = false;

  m_particle_top_pt      = -99;
  m_particle_top_eta     = -99;
  m_particle_top_phi     = -99;
  m_particle_top_e       = -99;
  m_particle_top_m       = -99;
  m_particle_top_y       = -99;

  m_particle_tbar_pt     = -99;
  m_particle_tbar_eta    = -99;
  m_particle_tbar_phi    = -99;
  m_particle_tbar_e      = -99;
  m_particle_tbar_m      = -99;
  m_particle_tbar_y      = -99;

  m_particle_av_top_pt      = -99;
  m_particle_av_top_eta     = -99;
  m_particle_av_top_phi     = -99;
  m_particle_av_top_e       = -99;
  m_particle_av_top_m       = -99;
  m_particle_av_top_y       = -99;

  m_particle_t1_pt      = -99;
  m_particle_t1_eta     = -99;
  m_particle_t1_phi     = -99;
  m_particle_t1_e       = -99;
  m_particle_t1_m       = -99;
  m_particle_t1_y       = -99;

  m_particle_t2_pt      = -99;
  m_particle_t2_eta     = -99;
  m_particle_t2_phi     = -99;
  m_particle_t2_e       = -99;
  m_particle_t2_m       = -99;
  m_particle_t2_y       = -99;

  m_particle_ttbar_pt    = -99;
  m_particle_ttbar_eta   = -99;
  m_particle_ttbar_phi   = -99;
  m_particle_ttbar_e     = -99;
  m_particle_ttbar_m     = -99;
  m_particle_ttbar_y     = -99;

  m_particle_cos_helicity_p   = -99.;
  m_particle_cos_helicity_m   = -99.;
  m_particle_cos_raxis_p      = -99.;
  m_particle_cos_raxis_m      = -99.;
  m_particle_cos_transverse_p = -99.;
  m_particle_cos_transverse_m = -99.;
  m_particle_opening_angle    = -99.;
  m_particle_phi_star_t_tbar  = -99.;
  
  m_particle_ttbar_dphi = -99.;
  m_particle_ttbar_pout = -99.;

  m_particle_nu_n        = 0;
  m_particle_nu_pt       = -99;
  m_particle_nu_eta      = -99;
  m_particle_nu_phi      = -99;
  m_particle_nu_e        = -99;
  m_particle_nu_m        = -99;
  m_particle_nu_y        = -99;

  m_particle_nubar_pt    = -99;
  m_particle_nubar_eta   = -99;
  m_particle_nubar_phi   = -99;
  m_particle_nubar_e     = -99;
  m_particle_nubar_m     = -99;
  m_particle_nubar_y     = -99;

  m_particle_Wp_pt       = -99;
  m_particle_Wp_eta      = -99;
  m_particle_Wp_phi      = -99;
  m_particle_Wp_e        = -99;
  m_particle_Wp_m        = -99;
  m_particle_Wp_y        = -99;

  m_particle_Wm_pt       = -99;
  m_particle_Wm_eta      = -99;
  m_particle_Wm_phi      = -99;
  m_particle_Wm_e        = -99;
  m_particle_Wm_m        = -99;
  m_particle_Wm_y        = -99;
} 

float TopDileptonEventSaver::phi_star(TLorentzVector matter, TLorentzVector anti_matter){

  float phi_acop = TMath::Pi() - matter.DeltaPhi(anti_matter); // Should this be anti_matter.DeltaPhi(matter)? Does it matter? (lol)
  float theta_star = TMath::ACos(TMath::TanH((matter.Eta() - anti_matter.Eta())/2.));
  float phi_star = TMath::Tan(phi_acop/2.)*TMath::Sin(theta_star);
  return phi_star;  
}

float TopDileptonEventSaver::cos_theta_helicity(TLorentzVector top, TLorentzVector parent_t, TLorentzVector ttbar, TLorentzVector lep, float sign){

  TVector3 boost_to_ttbar = ttbar.BoostVector();
  boost_to_ttbar *= -1.;
    
  parent_t.Boost(boost_to_ttbar);
  top.Boost(boost_to_ttbar);
  lep.Boost(boost_to_ttbar);
    
  TVector3 boost_to_parent_t = parent_t.BoostVector();
  boost_to_parent_t *= -1.;

  lep.Boost(boost_to_parent_t);
  
  TVector3 k_vector = top.Vect().Unit();
  k_vector *= sign;
  float theta = lep.Vect().Unit().Dot(k_vector);  

  //-- If we have a nan move to this to the overflow bins --//
  if (isnan(theta)){
    return -55.;
  } else {
    return theta;
  }
}

float TopDileptonEventSaver::cos_theta_raxis(TLorentzVector top, TLorentzVector parent_t, TLorentzVector ttbar, TLorentzVector lep, float sign){

  TVector3 boost_to_ttbar = ttbar.BoostVector();
  boost_to_ttbar *= -1.;
    
  parent_t.Boost(boost_to_ttbar);
  top.Boost(boost_to_ttbar);
  lep.Boost(boost_to_ttbar);
    
  TVector3 boost_to_parent_t = parent_t.BoostVector();
  boost_to_parent_t *= -1.;

  lep.Boost(boost_to_parent_t);
  
  TVector3 k_vector = top.Vect().Unit();
  k_vector *= sign;
  TVector3 p_vector(0,0,1);

  float y = p_vector.Dot(k_vector);
  float r = pow((1. - y*y),0.5);

  TVector3 r_vector = (1./r)*(p_vector - y*k_vector);
  if (sign == 1){
    ///-- We're in the a axis --///
    if(y > 0) r_vector *= 1.;
    if(y < 0) r_vector *= -1.;
  } else if (sign == -1){
    ///-- We're in the b axis --///
    if(y > 0) r_vector *= -1.;
    if(y < 0) r_vector *= 1.;
  }

  float theta = lep.Vect().Unit().Dot(r_vector);

  //-- If we have a nan move to this to the overflow bins --//
  if (isnan(theta)){
    return -55.;
  } else {
    return theta;
  }
}

float TopDileptonEventSaver::cos_theta_transverse(TLorentzVector top, TLorentzVector parent_t, TLorentzVector ttbar, TLorentzVector lep, float sign){

  TVector3 boost_to_ttbar = ttbar.BoostVector();
  boost_to_ttbar *= -1.;
    
  parent_t.Boost(boost_to_ttbar);
  top.Boost(boost_to_ttbar);
  lep.Boost(boost_to_ttbar);
    
  TVector3 boost_to_parent_t = parent_t.BoostVector();
  boost_to_parent_t *= -1.;

  lep.Boost(boost_to_parent_t);
  
  TVector3 k_vector = top.Vect().Unit();
  k_vector *= sign;
  TVector3 p_vector(0,0,1);

  float y = p_vector.Dot(k_vector);
  float r = pow((1. - y*y),0.5);

  TVector3 n_vector = (1./r)*(p_vector.Cross(k_vector)); ///-- Should this be Unit Vector? --///

  if (sign == 1){
    ///-- We're in the a axis --///
    if(y > 0) n_vector *= 1.;
    if(y < 0) n_vector *= -1.;
  } else if (sign == -1){
    ///-- We're in the b axis --///
    if(y > 0) n_vector *= -1.;
    if(y < 0) n_vector *= 1.;
  }

  float theta = lep.Vect().Unit().Dot(n_vector);

  //-- If we have a nan move to this to the overflow bins --//
  if (isnan(theta)){
    return -55.;
  } else {
    return theta;
  }
}

float TopDileptonEventSaver::opening_angle(TLorentzVector ttbar, TLorentzVector lep_p, TLorentzVector lep_n){

  TVector3 boost_to_ttbar = ttbar.BoostVector();
  boost_to_ttbar *= -1.;
    
  lep_p.Boost(boost_to_ttbar);
  lep_n.Boost(boost_to_ttbar);  

  float phi = lep_p.DeltaPhi(lep_n);
  return phi;
}


float TopDileptonEventSaver::calculateCosCos(TLorentzVector t, TLorentzVector tbar, TLorentzVector lp, TLorentzVector lm){

  TLorentzVector ttbar = t + tbar;

  TVector3 boost_to_ttbar = ttbar.BoostVector();
  boost_to_ttbar *= -1.;
    
  t.Boost(boost_to_ttbar);
  tbar.Boost(boost_to_ttbar);
  lp.Boost(boost_to_ttbar);
  lm.Boost(boost_to_ttbar);
    
  TVector3 boost_to_top = t.BoostVector();
  boost_to_top *= -1.;

  lp.Boost(boost_to_top);
    
  TVector3 boost_to_tbar = tbar.BoostVector();
  boost_to_tbar *= -1.;

  lm.Boost(boost_to_tbar);
    
  float c = lp.Vect().Unit().Dot(t.Vect().Unit());
  TVector3 top_vec = t.Vect().Unit();
  top_vec *= -1.;
  float d = lm.Vect().Unit().Dot(top_vec);

  //-- If we have a nan move to this to the overflow bins --//
  if (isnan(c) || isnan(d)){
    return -55.;
  }
  else {
    return c * d;
  }
}

float TopDileptonEventSaver::calculateDphi_ttbar(TLorentzVector t, TLorentzVector tbar){
  return fabs(t.DeltaPhi(tbar));
}

float TopDileptonEventSaver::calculatePout(TLorentzVector t, TLorentzVector tbar){
  TVector3 t3(t.X(),t.Y(),t.Z());
  TVector3 tbar3(tbar.X(),tbar.Y(),tbar.Z());
  TVector3 z(0,0,1);
  TVector3 vec_temp=tbar3.Cross(z);
  vec_temp*=1/vec_temp.Mag();
  float pout=t3.Dot(vec_temp);
  return pout;
}


//  LocalWords:  coscos

//}
