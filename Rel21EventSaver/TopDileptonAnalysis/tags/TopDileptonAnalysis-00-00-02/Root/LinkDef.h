#include "TopDileptonAnalysis/TopDileptonEventSaver.h"
#include "TopDileptonAnalysis/NeutrinoWeighter.h"

#ifdef __CINT__

#pragma extra_include "TopDileptonAnalysis/TopDileptonEventSaver.h";
#pragma extra_include "TopDileptonAnalysis/NeutrinoWeighter.h";

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class NeutrinoWeighter+;
#pragma link C++ class TopDileptonEventSaver+;


#endif
