#ifndef ANALYSISTOP_TOPPARTONS_CALCTTBARPARTONHISTORY_H
#define ANALYSISTOP_TOPPARTONS_CALCTTBARPARTONHISTORY_H

/**
  * @author John Morris <john.morris@cern.ch>
  * @author Silvestre Marino Romano <sromanos@cern.ch>
  * @author Samuel Calvet <scalvet@cern.ch>
  * 
  * @brief CalcTtbarPartonHistory
  *   Class derived from CalcTopPartonHistory, used to store ttbar variables
  * 
  * $Revision: 259923 $
  * $Date: 2017-03-28 12:51:53 +0200 (Tue, 28 Mar 2017) $
  * 
  **/ 


// Framework include(s):
#include "TopPartons/CalcTopPartonHistory.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TopPartons/PartonHistory.h"

// forward declaration(s):
namespace top{
  class TopConfig;
}

namespace top{

  class CalcTtbarPartonHistory : public CalcTopPartonHistory{
    public:      
      explicit CalcTtbarPartonHistory( const std::string& name );
      virtual ~CalcTtbarPartonHistory() {}
      
      //Storing parton history for ttbar resonance analysis      
      CalcTtbarPartonHistory(const CalcTtbarPartonHistory& rhs) = delete;
      CalcTtbarPartonHistory(CalcTtbarPartonHistory&& rhs) = delete;
      CalcTtbarPartonHistory& operator=(const CalcTtbarPartonHistory& rhs) = delete; 
      
      void ttbarHistorySaver(const xAOD::TruthParticleContainer* truthParticles, xAOD::PartonHistory* ttbarPartonHistory); 
             
      virtual StatusCode execute();
      
  };
  
}

#endif
