// $Id: CalcTopPartonHistory.h 259923 2017-03-28 10:51:53Z jhowarth $
#ifndef ANALYSISTOP_TOPPARTONS_CALCTOPPARTONHISTORY_H
#define ANALYSISTOP_TOPPARTONS_CALCTOPPARTONHISTORY_H

/**
  * @author John Morris <john.morris@cern.ch>
  * @author Silvestre Marino Romano <sromanos@cern.ch>
  * @author Samuel Calvet <scalvet@cern.ch>
  * 
  * @brief CalcTopPartonHistory
  *   Determine the history of the top partons
  * 
  * $Revision: 259923 $
  * $Date: 2017-03-28 12:51:53 +0200 (Tue, 28 Mar 2017) $
  * 
  **/ 

// system include(s):
#include <memory>
#include <vector>

// Framework include(s):
#include "AsgTools/AsgTool.h"
#include "AthContainers/DataVector.h"
#include "AthContainers/AuxElement.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TopPartons/PartonHistory.h"

// forward declaration(s):
namespace top{
  class TopConfig;
}

namespace top{
  
  class CalcTopPartonHistory : public asg::AsgTool {
    public:
      explicit CalcTopPartonHistory( const std::string& name );
      virtual ~CalcTopPartonHistory() {}

      CalcTopPartonHistory(const CalcTopPartonHistory& rhs) = delete;
      CalcTopPartonHistory(CalcTopPartonHistory&& rhs) = delete;
      CalcTopPartonHistory& operator=(const CalcTopPartonHistory& rhs) = delete;
     
      ///Store the four-momentum of several particles in the top decay chain

      bool topWb( const xAOD::TruthParticleContainer* truthParticles, int start_pdgId,
		  TLorentzVector& t_beforeFSR, TLorentzVector& t_afterFSR,
		  TLorentzVector& W_beforeFSR, TLorentzVector& W_afterFSR,
		  TLorentzVector& b_beforeFSR, TLorentzVector& b_afterFSR,
		  TLorentzVector& Wdecay1_beforeFSR, TLorentzVector& Wdecay1_afterFSR, int& Wdecay1_pdgId,
		  TLorentzVector& Wdecay2_beforeFSR, TLorentzVector& Wdecay2_afterFSR, int& Wdecay2_pdgId);

      bool topBoson( const xAOD::TruthParticleContainer* truthParticles, int start_pdgId,
		     TLorentzVector& boson_beforeFSR, TLorentzVector& boson_afterFSR,
		     TLorentzVector& bosondecay1_beforeFSR, TLorentzVector& bosondecay1_afterFSR, int& bosondecay1_pdgId,
		     TLorentzVector& bosondecay2_beforeFSR, TLorentzVector& bosondecay2_afterFSR, int& bosondecay2_pdgId);

      ///Store the four-momentum of b (not from tops_ before and after FSR
      bool b(const xAOD::TruthParticleContainer* truthParticles, TLorentzVector& b_beforeFSR, TLorentzVector& b_afterFSR);
      
      ///Store the four-momentum of several particles in the W decay chain
      bool Wlv(const xAOD::TruthParticleContainer* truthParticles, TLorentzVector& W_p4, TLorentzVector& Wdecay1_p4, int& Wdecay1_pdgId, TLorentzVector& Wdecay2_p4, int& Wdecay2_pdgId);
      
      ///Return particle after FSR (before the decay vertex)
      const xAOD::TruthParticle* find_last_in_chain(const xAOD::TruthParticle* particle);
      
      ///Return true when particle is a top before FSR
      bool hasParticleIdenticalParent(const xAOD::TruthParticle* particle);
      bool has_identical_parent(const xAOD::TruthParticle* particle);

      virtual StatusCode execute(); 
    protected:
      std::shared_ptr<top::TopConfig> m_config;
      
      void fillEtaBranch(xAOD::PartonHistory* partonHistory,std:: string branchName, TLorentzVector &tlv);
      
  }; 
}

#endif
