// $Id: PartonHistory.h 259923 2017-03-28 10:51:53Z jhowarth $
#ifndef ANALYSISTOP_TOPPARTONS_PARTONHISORTY_H
#define ANALYSISTOP_TOPPARTONS_PARTONHISORTY_H

/**
  * @author John Morris <john.morris@cern.ch>
  * @author Silvestre Marino Romano <sromanos@cern.ch>
  * @author Samuel Calvet <scalvet@cern.ch>
  * 
  * @brief PartonHistory
  *   A simple xAOD class which we can persist into a mini-xAOD
  *   The xAOD EDM is way too complex, so let's simplify it
  *   It's not like ROOT can do schema evolution......
  * 
  *   In order for this object to be used by any
  *   PartonHistory factory, it contains no AuxElements
  *   Everything should be done via decoration
  * 
  *   This really should be an AuxInfoBase type of xAOD object
  *   But John can't figure out how to copy these to the 
  *   output mini-xAOD.....yet
  * 
  * $Revision: 259923 $
  * $Date: 2017-03-28 12:51:53 +0200 (Tue, 28 Mar 2017) $
  * 
  **/ 

// EDM include(s):
#include "AthContainers/DataVector.h"
#include "AthContainers/AuxElement.h"
#include "xAODCore/AuxContainerBase.h"
#include "xAODCore/CLASS_DEF.h"
#include <vector>

namespace xAOD{

  /// Aux Container 
  class PartonHistoryAuxContainer : public AuxContainerBase {
    public:
      /// Default constructor
      PartonHistoryAuxContainer();     
  }; // end Aux container
  
  /// Interface class
  class PartonHistory : public SG::AuxElement {
    public:
      /// Default constructor
      PartonHistory();
      /// Default desturctor
      virtual ~PartonHistory(){}    

      void IniVarTtbar();
      void IniVarHiggs();
      void IniVarZBoson();
      void IniVarWBoson();
      void IniVarGluon();
      void IniVarTbbar();
      void IniVarWlv();
         
  };   
  typedef DataVector < xAOD::PartonHistory > PartonHistoryContainer;   
}

// Dictonaries
CLASS_DEF( xAOD::PartonHistory , 135846343 , 1 )
CLASS_DEF( xAOD::PartonHistoryContainer , 1219079565 , 1 )
CLASS_DEF( xAOD::PartonHistoryAuxContainer , 1244378748 , 1 )

#endif
