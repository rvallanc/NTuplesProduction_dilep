#ifndef ANALYSISTOP_TOPPARTONS_CALCWLVPARTONHISTORY_H
#define ANALYSISTOP_TOPPARTONS_CALCWLVPARTONHISTORY_H

/**
  * @author John Morris <john.morris@cern.ch>
  * @author Silvestre Marino Romano <sromanos@cern.ch>
  * @author Samuel Calvet <scalvet@cern.ch>
  * 
  * @brief CalcWlvPartonHistory
  *   Class derived from CalcTopPartonHistory, used to store ttbar variables
  * 
  * $Revision: 259923 $
  * $Date: 2017-03-28 12:51:53 +0200 (Tue, 28 Mar 2017) $
  * 
  **/ 


// Framework include(s):
#include "TopPartons/CalcTopPartonHistory.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "TopPartons/PartonHistory.h"

// forward declaration(s):
namespace top{
  class TopConfig;
}

namespace top{

  class CalcWlvPartonHistory : public CalcTopPartonHistory{
    public:      
      explicit CalcWlvPartonHistory( const std::string& name );
      virtual ~CalcWlvPartonHistory() {}
      
      //Storing parton history for ttbar resonance analysis      
      CalcWlvPartonHistory(const CalcWlvPartonHistory& rhs) = delete;
      CalcWlvPartonHistory(CalcWlvPartonHistory&& rhs) = delete;
      CalcWlvPartonHistory& operator=(const CalcWlvPartonHistory& rhs) = delete; 
      
      void WlvHistorySaver(const xAOD::TruthParticleContainer* truthParticles, xAOD::PartonHistory* wlvPartonHistory); 
             
      virtual StatusCode execute();
      
  };
  
}

#endif
