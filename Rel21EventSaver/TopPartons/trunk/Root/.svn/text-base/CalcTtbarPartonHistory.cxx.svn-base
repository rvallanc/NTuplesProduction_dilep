#include "TopPartons/CalcTtbarPartonHistory.h"
#include "TopConfiguration/TopConfig.h"

namespace top{
  
  CalcTtbarPartonHistory::CalcTtbarPartonHistory(const std::string& name) : CalcTopPartonHistory( name ){}
  
  void CalcTtbarPartonHistory::ttbarHistorySaver(const xAOD::TruthParticleContainer* truthParticles, xAOD::PartonHistory* ttbarPartonHistory){
    
    //std::cout << "ttbarHistorySaver: Initiating input trees" << std::endl;
     ttbarPartonHistory->IniVarTtbar();
     ttbarPartonHistory->IniVarHiggs();
     ttbarPartonHistory->IniVarZBoson();
     ttbarPartonHistory->IniVarGluon();

     TLorentzVector t_before_FSR, t_after_FSR;
     TLorentzVector b_before_FSR, b_after_FSR;
     TLorentzVector W_from_t_before_FSR, W_from_t_after_FSR;
     TLorentzVector W_from_t_decay1_before_FSR, W_from_t_decay1_after_FSR;
     TLorentzVector W_from_t_decay2_before_FSR, W_from_t_decay2_after_FSR;
     int W_from_t_decay1_pdgId;
     int W_from_t_decay2_pdgId;

     //std::cout << "ttbarHistorySaver: calculating topWb(6)" << std::endl;
     bool event_top = CalcTopPartonHistory::topWb(truthParticles, 6, 
						  t_before_FSR, t_after_FSR,
						  b_before_FSR, b_after_FSR,
						  W_from_t_before_FSR, W_from_t_after_FSR,
						  W_from_t_decay1_before_FSR, W_from_t_decay1_after_FSR, W_from_t_decay1_pdgId,
						  W_from_t_decay2_before_FSR, W_from_t_decay2_after_FSR, W_from_t_decay2_pdgId);

     TLorentzVector tbar_before_FSR, tbar_after_FSR;
     TLorentzVector bbar_before_FSR, bbar_after_FSR;
     TLorentzVector W_from_tbar_before_FSR, W_from_tbar_after_FSR;
     TLorentzVector W_from_tbar_decay1_before_FSR, W_from_tbar_decay1_after_FSR;
     TLorentzVector W_from_tbar_decay2_before_FSR, W_from_tbar_decay2_after_FSR;
     int W_from_tbar_decay1_pdgId;
     int W_from_tbar_decay2_pdgId;

     bool event_tbar = CalcTopPartonHistory::topWb(truthParticles, -6, 
						   tbar_before_FSR, tbar_after_FSR,
						   bbar_before_FSR, bbar_after_FSR,
						   W_from_tbar_before_FSR, W_from_tbar_after_FSR,
						   W_from_tbar_decay1_before_FSR, W_from_tbar_decay1_after_FSR, W_from_tbar_decay1_pdgId,
						   W_from_tbar_decay2_before_FSR, W_from_tbar_decay2_after_FSR, W_from_tbar_decay2_pdgId);

     ///-- Extra Boson Stuff --///
     TLorentzVector H_before_FSR, H_after_FSR;
     TLorentzVector H_decay1_before_FSR, H_decay1_after_FSR; 
     TLorentzVector H_decay2_before_FSR, H_decay2_after_FSR; 
     int H_decay1_pdgId;
     int H_decay2_pdgId;

     bool event_H = CalcTopPartonHistory::topBoson(truthParticles, 25, 
						       H_before_FSR, H_after_FSR,
						       H_decay1_before_FSR, H_decay1_after_FSR, H_decay1_pdgId,
						       H_decay2_before_FSR, H_decay2_after_FSR, H_decay2_pdgId);

     TLorentzVector Z_before_FSR, Z_after_FSR;
     TLorentzVector Z_decay1_before_FSR, Z_decay1_after_FSR; 
     TLorentzVector Z_decay2_before_FSR, Z_decay2_after_FSR; 
     int Z_decay1_pdgId;
     int Z_decay2_pdgId;

     bool event_Z = CalcTopPartonHistory::topBoson(truthParticles, 23, 
						   Z_before_FSR, Z_after_FSR,
						   Z_decay1_before_FSR, Z_decay1_after_FSR, Z_decay1_pdgId,
						   Z_decay2_before_FSR, Z_decay2_after_FSR, Z_decay2_pdgId);

     TLorentzVector gluon_before_FSR, gluon_after_FSR;
     TLorentzVector gluon_decay1_before_FSR, gluon_decay1_after_FSR; 
     TLorentzVector gluon_decay2_before_FSR, gluon_decay2_after_FSR; 
     int gluon_decay1_pdgId;
     int gluon_decay2_pdgId;

     //std::cout << "ttbarHistorySaver: calculating topBoson(21)" << std::endl;
     bool event_gluon = CalcTopPartonHistory::topBoson(truthParticles, 21, 
						       gluon_before_FSR, gluon_after_FSR,
						       gluon_decay1_before_FSR, gluon_decay1_after_FSR, gluon_decay1_pdgId,
						       gluon_decay2_before_FSR, gluon_decay2_after_FSR, gluon_decay2_pdgId);
     
     
     if (event_gluon){
       ttbarPartonHistory->auxdecor< float >( "gluon_before_FSR_m" )     = gluon_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "gluon_before_FSR_pt" )    = gluon_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "gluon_before_FSR_phi" )   = gluon_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >(   "gluon_before_FSR_pdgId" ) = 21;       
       fillEtaBranch(ttbarPartonHistory, "gluon_before_FSR_eta", gluon_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "gluon_after_FSR_m" )     = gluon_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "gluon_after_FSR_pt" )    = gluon_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "gluon_after_FSR_phi" )   = gluon_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >(   "gluon_after_FSR_pdgId" ) = 21;       
       fillEtaBranch(ttbarPartonHistory, "gluon_after_FSR_eta", gluon_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "gluon_decay1_before_FSR_m" )     = gluon_decay1_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "gluon_decay1_before_FSR_pt" )    = gluon_decay1_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "gluon_decay1_before_FSR_phi" )   = gluon_decay1_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >(   "gluon_decay1_before_FSR_pdgId" ) = gluon_decay1_pdgId;
       fillEtaBranch(ttbarPartonHistory, "gluon_decay1_before_FSR_eta", gluon_decay1_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "gluon_decay1_after_FSR_m" )      = gluon_decay1_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "gluon_decay1_after_FSR_pt" )     = gluon_decay1_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "gluon_decay1_after_FSR_phi" )    = gluon_decay1_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >(   "gluon_decay1_after_FSR_pdgId" )  = gluon_decay1_pdgId;
       fillEtaBranch(ttbarPartonHistory, "gluon_decay1_after_FSR_eta", gluon_decay1_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "gluon_decay2_before_FSR_m" )     = gluon_decay2_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "gluon_decay2_before_FSR_pt" )    = gluon_decay2_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "gluon_decay2_before_FSR_phi" )   = gluon_decay2_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >(   "gluon_decay2_before_FSR_pdgId" ) = gluon_decay2_pdgId;
       fillEtaBranch(ttbarPartonHistory, "gluon_decay2_before_FSR_eta", gluon_decay2_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "gluon_decay2_after_FSR_m" )      = gluon_decay2_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "gluon_decay2_after_FSR_pt" )     = gluon_decay2_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "gluon_decay2_after_FSR_phi" )    = gluon_decay2_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >(   "gluon_decay2_after_FSR_pdgId" )  = gluon_decay2_pdgId;
       fillEtaBranch(ttbarPartonHistory, "gluon_decay2_after_FSR_eta", gluon_decay2_after_FSR);
     }

     if (event_Z){
       ttbarPartonHistory->auxdecor< float >( "Z_before_FSR_m" )   = Z_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "Z_before_FSR_pt" )  = Z_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "Z_before_FSR_phi" ) = Z_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "Z_before_FSR_pdgId" ) = 23;
       fillEtaBranch(ttbarPartonHistory, "Z_before_FSR_eta", Z_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "Z_after_FSR_m" )   = Z_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "Z_after_FSR_pt" )  = Z_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "Z_after_FSR_phi" ) = Z_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "Z_after_FSR_pdgId" ) = 23;
       fillEtaBranch(ttbarPartonHistory, "Z_after_FSR_eta", Z_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "Z_decay1_before_FSR_m" )   = Z_decay1_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "Z_decay1_before_FSR_pt" )  = Z_decay1_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "Z_decay1_before_FSR_phi" ) = Z_decay1_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "Z_decay1_before_FSR_pdgId" ) = Z_decay1_pdgId;
       fillEtaBranch(ttbarPartonHistory, "Z_decay1_before_FSR_eta", Z_decay1_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "Z_decay1_after_FSR_m" )   = Z_decay1_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "Z_decay1_after_FSR_pt" )  = Z_decay1_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "Z_decay1_after_FSR_phi" ) = Z_decay1_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "Z_decay1_after_FSR_pdgId" ) = Z_decay1_pdgId;
       fillEtaBranch(ttbarPartonHistory, "Z_decay1_after_FSR_eta", Z_decay1_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "Z_decay2_before_FSR_m" )   = Z_decay2_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "Z_decay2_before_FSR_pt" )  = Z_decay2_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "Z_decay2_before_FSR_phi" ) = Z_decay2_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "Z_decay2_before_FSR_pdgId" ) = Z_decay2_pdgId;
       fillEtaBranch(ttbarPartonHistory, "Z_decay2_before_FSR_eta", Z_decay2_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "Z_decay2_after_FSR_m" )   = Z_decay2_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "Z_decay2_after_FSR_pt" )  = Z_decay2_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "Z_decay2_after_FSR_phi" ) = Z_decay2_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "Z_decay2_after_FSR_pdgId" ) = Z_decay2_pdgId;
       fillEtaBranch(ttbarPartonHistory, "Z_decay2_after_FSR_eta", Z_decay2_after_FSR);
     }

     if (event_H){
       ttbarPartonHistory->auxdecor< float >( "H_before_FSR_m" )   = H_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "H_before_FSR_pt" )  = H_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "H_before_FSR_phi" ) = H_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "H_before_FSR_pdgId" ) = 25;
       fillEtaBranch(ttbarPartonHistory, "H_before_FSR_eta", H_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "H_after_FSR_m" )   = H_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "H_after_FSR_pt" )  = H_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "H_after_FSR_phi" ) = H_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "H_after_FSR_pdgId" ) = 25;
       fillEtaBranch(ttbarPartonHistory, "H_after_FSR_eta", H_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "H_decay1_before_FSR_m" )   = H_decay1_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "H_decay1_before_FSR_pt" )  = H_decay1_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "H_decay1_before_FSR_phi" ) = H_decay1_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "H_decay1_before_FSR_pdgId" ) = H_decay1_pdgId;
       fillEtaBranch(ttbarPartonHistory, "H_decay1_before_FSR_eta", H_decay1_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "H_decay1_after_FSR_m" )   = H_decay1_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "H_decay1_after_FSR_pt" )  = H_decay1_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "H_decay1_after_FSR_phi" ) = H_decay1_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "H_decay1_after_FSR_pdgId" ) = H_decay1_pdgId;
       fillEtaBranch(ttbarPartonHistory, "H_decay1_after_FSR_eta", H_decay1_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "H_decay2_before_FSR_m" )   = H_decay2_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "H_decay2_before_FSR_pt" )  = H_decay2_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "H_decay2_before_FSR_phi" ) = H_decay2_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "H_decay2_before_FSR_pdgId" ) = H_decay2_pdgId;
       fillEtaBranch(ttbarPartonHistory, "H_decay2_before_FSR_eta", H_decay2_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "H_decay2_after_FSR_m" )   = H_decay2_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "H_decay2_after_FSR_pt" )  = H_decay2_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "H_decay2_after_FSR_phi" ) = H_decay2_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "H_decay2_after_FSR_pdgId" ) = H_decay2_pdgId;
       fillEtaBranch(ttbarPartonHistory, "H_decay2_after_FSR_eta", H_decay2_after_FSR);
     }

     if (event_top && event_tbar){
       //std::cout << "ttbarHistorySaver: Filling ttbar" << std::endl;
       TLorentzVector temp = t_before_FSR + tbar_before_FSR;
       ttbarPartonHistory->auxdecor< float >( "ttbar_before_FSR_m" )   = temp.M();
       ttbarPartonHistory->auxdecor< float >( "ttbar_before_FSR_pt" )  = temp.Pt();
       ttbarPartonHistory->auxdecor< float >( "ttbar_before_FSR_phi" ) = temp.Phi();
       fillEtaBranch(ttbarPartonHistory,"ttbar_before_FSR_eta", temp);
       
       temp = t_after_FSR + tbar_after_FSR;
       ttbarPartonHistory->auxdecor< float >( "ttbar_after_FSR_m" ) = temp.M();
       ttbarPartonHistory->auxdecor< float >( "ttbar_after_FSR_pt" ) = temp.Pt();
       ttbarPartonHistory->auxdecor< float >( "ttbar_after_FSR_phi" ) = temp .Phi();
       fillEtaBranch(ttbarPartonHistory, "ttbar_after_FSR_eta", temp);
     }
       
     if (event_top){
       //std::cout << "ttbarHistorySaver: Filling top" << std::endl;
       ttbarPartonHistory->auxdecor< float >( "t_before_FSR_m" )   = t_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "t_before_FSR_pt" )  = t_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "t_before_FSR_phi" ) = t_before_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory,"t_before_FSR_eta", t_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "t_after_FSR_m" )   = t_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "t_after_FSR_pt" )  = t_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "t_after_FSR_phi" ) = t_after_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory,"t_after_FSR_eta", t_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "b_from_t_before_FSR_m" )   = b_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "b_from_t_before_FSR_pt" )  = b_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "b_from_t_before_FSR_phi" ) = b_before_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory,"b_from_t_before_FSR_eta", b_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "b_from_t_after_FSR_m" )   = b_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "b_from_t_after_FSR_pt" )  = b_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "b_from_t_after_FSR_phi" ) = b_after_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory,"b_from_t_after_FSR_eta", b_after_FSR);
       
       ttbarPartonHistory->auxdecor< float >( "W_from_t_before_FSR_m" )   = W_from_t_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_before_FSR_pt" )  = W_from_t_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_before_FSR_phi" ) = W_from_t_before_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory, "W_from_t_before_FSR_eta", W_from_t_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "W_from_t_after_FSR_m" )   = W_from_t_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_after_FSR_pt" )  = W_from_t_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_after_FSR_phi" ) = W_from_t_after_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory, "W_from_t_after_FSR_eta", W_from_t_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay1_before_FSR_m" )   = W_from_t_decay1_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay1_before_FSR_pt" )  = W_from_t_decay1_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay1_before_FSR_phi" ) = W_from_t_decay1_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "W_from_t_decay1_before_FSR_pdgId" ) = W_from_t_decay1_pdgId;
       fillEtaBranch(ttbarPartonHistory, "W_from_t_decay1_before_FSR_eta", W_from_t_decay1_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay1_after_FSR_m" )   = W_from_t_decay1_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay1_after_FSR_pt" )  = W_from_t_decay1_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay1_after_FSR_phi" ) = W_from_t_decay1_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "W_from_t_decay1_after_FSR_pdgId" ) = W_from_t_decay1_pdgId;
       fillEtaBranch(ttbarPartonHistory, "W_from_t_decay1_after_FSR_eta", W_from_t_decay1_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay2_before_FSR_m" )   = W_from_t_decay2_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay2_before_FSR_pt" )  = W_from_t_decay2_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay2_before_FSR_phi" ) = W_from_t_decay2_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "W_from_t_decay2_before_FSR_pdgId" ) = W_from_t_decay2_pdgId;
       fillEtaBranch(ttbarPartonHistory, "W_from_t_decay2_before_FSR_eta", W_from_t_decay2_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay2_after_FSR_m" )   = W_from_t_decay2_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay2_after_FSR_pt" )  = W_from_t_decay2_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_t_decay2_after_FSR_phi" ) = W_from_t_decay2_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "W_from_t_decay2_after_FSR_pdgId" ) = W_from_t_decay2_pdgId;
       fillEtaBranch(ttbarPartonHistory, "W_from_t_decay2_after_FSR_eta", W_from_t_decay2_after_FSR);
     }

     if (event_tbar){
       //std::cout << "ttbarHistorySaver: Filling tbar" << std::endl;
       ttbarPartonHistory->auxdecor< float >( "tbar_before_FSR_m" )   = tbar_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "tbar_before_FSR_pt" )  = tbar_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "tbar_before_FSR_phi" ) = tbar_before_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory,"tbar_before_FSR_eta", tbar_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "tbar_after_FSR_m" )   = tbar_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "tbar_after_FSR_pt" )  = tbar_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "tbar_after_FSR_phi" ) = tbar_after_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory,"tbar_after_FSR_eta", tbar_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "bbar_from_tbar_before_FSR_m" )   = bbar_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "bbar_from_tbar_before_FSR_pt" )  = bbar_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "bbar_from_tbar_before_FSR_phi" ) = bbar_before_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory,"bbar_from_tbar_before_FSR_eta", bbar_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "bbar_from_tbar_after_FSR_m" )   = bbar_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "bbar_from_tbar_after_FSR_pt" )  = bbar_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "bbar_from_tbar_after_FSR_phi" ) = bbar_after_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory,"bbar_from_tbar_after_FSR_eta", bbar_after_FSR);
       
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_before_FSR_m" )   = W_from_tbar_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_before_FSR_pt" )  = W_from_tbar_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_before_FSR_phi" ) = W_from_tbar_before_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory, "W_from_tbar_before_FSR_eta", W_from_tbar_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_after_FSR_m" )   = W_from_tbar_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_after_FSR_pt" )  = W_from_tbar_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_after_FSR_phi" ) = W_from_tbar_after_FSR.Phi();
       fillEtaBranch(ttbarPartonHistory, "W_from_tbar_after_FSR_eta", W_from_tbar_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay1_before_FSR_m" )   = W_from_tbar_decay1_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay1_before_FSR_pt" )  = W_from_tbar_decay1_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay1_before_FSR_phi" ) = W_from_tbar_decay1_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "W_from_tbar_decay1_before_FSR_pdgId" ) = W_from_tbar_decay1_pdgId;
       fillEtaBranch(ttbarPartonHistory, "W_from_tbar_decay1_before_FSR_eta", W_from_tbar_decay1_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay1_after_FSR_m" )   = W_from_tbar_decay1_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay1_after_FSR_pt" )  = W_from_tbar_decay1_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay1_after_FSR_phi" ) = W_from_tbar_decay1_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "W_from_tbar_decay1_after_FSR_pdgId" ) = W_from_tbar_decay1_pdgId;
       fillEtaBranch(ttbarPartonHistory, "W_from_tbar_decay1_after_FSR_eta", W_from_tbar_decay1_after_FSR);

       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay2_before_FSR_m" )   = W_from_tbar_decay2_before_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay2_before_FSR_pt" )  = W_from_tbar_decay2_before_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay2_before_FSR_phi" ) = W_from_tbar_decay2_before_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "W_from_tbar_decay2_before_FSR_pdgId" ) = W_from_tbar_decay2_pdgId;
       fillEtaBranch(ttbarPartonHistory, "W_from_tbar_decay2_before_FSR_eta", W_from_tbar_decay2_before_FSR);

       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay2_after_FSR_m" )   = W_from_tbar_decay2_after_FSR.M();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay2_after_FSR_pt" )  = W_from_tbar_decay2_after_FSR.Pt();
       ttbarPartonHistory->auxdecor< float >( "W_from_tbar_decay2_after_FSR_phi" ) = W_from_tbar_decay2_after_FSR.Phi();
       ttbarPartonHistory->auxdecor< int >( "W_from_tbar_decay2_after_FSR_pdgId" ) = W_from_tbar_decay2_pdgId;
       fillEtaBranch(ttbarPartonHistory, "W_from_tbar_decay2_after_FSR_eta", W_from_tbar_decay2_after_FSR);
     }
     
  }

  StatusCode CalcTtbarPartonHistory::execute()
  {
    //std::cout << "ttbarHistorySaver: excecute()" << std::endl;

     // Get the Truth Particles
     const xAOD::TruthParticleContainer* truthParticles(nullptr);
     ATH_CHECK( evtStore()->retrieve( truthParticles , m_config->sgKeyMCParticle() ) );
     //std::cout << "ttbarHistorySaver: retrieved truth particles" << std::endl;

     // Create the partonHistory xAOD object
     xAOD::PartonHistoryAuxContainer* partonAuxCont = new xAOD::PartonHistoryAuxContainer{};    
     xAOD::PartonHistoryContainer* partonCont = new xAOD::PartonHistoryContainer{};
     partonCont->setStore( partonAuxCont );
     //std::cout << "ttbarHistorySaver: creating partonHistory xAOD object" << std::endl;
  
     xAOD::PartonHistory* ttbarPartonHistory = new xAOD::PartonHistory{};
     partonCont->push_back( ttbarPartonHistory );
     //std::cout << "ttbarHistorySaver: creating ttbarPartonHistory" << std::endl;
     
     // Recover the parton history for ttbar events     
     ttbarHistorySaver(truthParticles, ttbarPartonHistory);     
     //std::cout << "ttbarHistorySaver: filled ttbarPartonHistory" << std::endl;
          
     // Save to StoreGate / TStore
     std::string outputSGKey = m_config->sgKeyTopPartonHistory();
     std::string outputSGKeyAux = outputSGKey + "Aux.";
      
     xAOD::TReturnCode save = evtStore()->tds()->record( partonCont , outputSGKey );
     xAOD::TReturnCode saveAux = evtStore()->tds()->record( partonAuxCont , outputSGKeyAux );
     //std::cout << "ttbarHistorySaver: saved ttbarPartonHistory" << std::endl;
     if( !save || !saveAux ){
       return StatusCode::FAILURE;
     }      
    
     return StatusCode::SUCCESS;
  } 
}
