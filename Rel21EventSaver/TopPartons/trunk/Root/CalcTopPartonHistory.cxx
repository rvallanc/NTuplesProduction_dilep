// $Id: CalcTopPartonHistory.cxx 259923 2017-03-28 10:51:53Z jhowarth $
#include "TopPartons/CalcTopPartonHistory.h"
#include "TopPartons/PartonHistory.h"
#include "TopConfiguration/TopConfig.h"
#include "TopPartons/CalcTtbarPartonHistory.h"

namespace top{
  
  CalcTopPartonHistory::CalcTopPartonHistory( const std::string& name ) :
    asg::AsgTool( name ),
    m_config(nullptr)
  {    
    declareProperty( "config" , m_config );
  }
  
  bool CalcTopPartonHistory::topWb( const xAOD::TruthParticleContainer* truthParticles, int start_pdgId, 
				    TLorentzVector& t_beforeFSR, TLorentzVector& t_afterFSR, 
				    TLorentzVector& b_beforeFSR, TLorentzVector& b_afterFSR,
				    TLorentzVector& W_beforeFSR, TLorentzVector& W_afterFSR, 
				    TLorentzVector& Wdecay1_beforeFSR, TLorentzVector& Wdecay1_afterFSR, int& Wdecay1_pdgId, 
				    TLorentzVector& Wdecay2_beforeFSR, TLorentzVector& Wdecay2_afterFSR, int& Wdecay2_pdgId){
    
    bool hasT		    = false;
    bool hasW		    = false;
    bool hasB		    = false;
    bool hasWdecayProd1     = false;
    bool hasWdecayProd2     = false;

    //std::cout << "begin topWb" << std::endl;

    if( fabs(start_pdgId) != 6 ){
      std::cout << "WARNING: You've asked for a PDGID that isn't the top (i.e. +/- 6), this code will do nothing" << std::endl;
      return false;
    }
    
    //std::cout << "Loop through truth particles" << std::endl;
    for (const xAOD::TruthParticle* particle : *truthParticles) {
    
      //std::cout << "Truth particle: " << particle->pdgId() << std::endl;
      if (particle->pdgId() != start_pdgId) continue;    
      //std::cout << "Checking if particle has identical parent" << std::endl;
      if (has_identical_parent(particle)) continue; // keeping only top before FSR			
      //std::cout << "Check complete, setting top particles" << std::endl;

      ///-- Set the top 4-vectors before and after FSR --///
      t_beforeFSR = particle->p4(); 
      particle   = find_last_in_chain(particle);
      t_afterFSR = particle->p4();
      hasT = true;		
      //std::cout << "Done setting top particles" << std::endl;
      
      ///-- Now look through the top decays --///
      for (size_t k = 0; k < particle->nChildren(); k++) {
	//std::cout << "top child particles: " << k << std::endl;
	const xAOD::TruthParticle* topChild = particle->child(k);	
	
	///-- W Boson --///
	if (fabs(topChild->pdgId()) == 24){				
	  //std::cout << "looking at W boson" << std::endl;
	  W_beforeFSR = topChild->p4();
	  topChild = find_last_in_chain(topChild);
	  W_afterFSR = topChild->p4();
	  hasW = true;
	  //std::cout << "finished looking at the W" << std::endl;
	  
	  ///-- Loop through W Boson decays --///
	  for (size_t q = 0; q < topChild->nChildren(); ++q) {
	    //std::cout << "Looking at W child number " << q << std::endl;
	    const xAOD::TruthParticle* WChild = topChild->child(q);
	    if (fabs(WChild->pdgId()) < 17) {
	      if (WChild->pdgId() > 0) {
		//std::cout << "Matter decay particle start" << std::endl;
		Wdecay1_beforeFSR = WChild->p4();
		Wdecay1_pdgId = WChild->pdgId();
		WChild = find_last_in_chain(WChild);
		hasWdecayProd1 = true;
		//std::cout << "Matter decay particle end" << std::endl;
	      } else {
		//std::cout << "Anti-Matter decay particle start" << std::endl;
		//std::cout << "Anti-Matter pdgId " << WChild->pdgId() << std::endl;
		Wdecay2_beforeFSR = WChild->p4();
		Wdecay2_pdgId = WChild->pdgId();
		//std::cout << "finding last in decay chain " << std::endl;
		WChild = find_last_in_chain(WChild);
		//std::cout << "finished last in decay chain " << std::endl;
		hasWdecayProd2 = true;
		//std::cout << "Anti-Matter decay particle end" << std::endl;
	      }
	    }						
	  }
	}
	///-- B quark --///
	else if (fabs(topChild->pdgId()) == 5) {
	  b_beforeFSR = topChild->p4();
	  topChild = find_last_in_chain(topChild);
	  b_afterFSR = topChild->p4();
	  hasB = true;
	} 	
      }
      //std::cout << "Finished looping through top children" << std::endl;
    }
    //    std::cout << "Returning status" << std::endl;
    return hasT && hasW && hasB && hasWdecayProd1 && hasWdecayProd2;
    
  }

  bool CalcTopPartonHistory::topBoson( const xAOD::TruthParticleContainer* truthParticles, int start_pdgId, 
				       TLorentzVector& boson_beforeFSR, TLorentzVector& boson_afterFSR, 
				       TLorentzVector& bosondecay1_beforeFSR, TLorentzVector& bosondecay1_afterFSR, int& bosondecay1_pdgId, 
				       TLorentzVector& bosondecay2_beforeFSR, TLorentzVector& bosondecay2_afterFSR, int& bosondecay2_pdgId){
    
    bool hasBoson	    = false;
    bool hasBosondecayProd1 = false;
    bool hasBosondecayProd2 = false;

    int counter = 0;
    for (const xAOD::TruthParticle* particle : *truthParticles) {
    
      if (particle->pdgId() != start_pdgId) continue;    
      if (has_identical_parent(particle)) continue; 
      if (counter > 20) continue; ///-- Bosons that we care about should be near start of truth record --///
      ++counter;

      ///-- Set the top 4-vectors before and after FSR --///
      //std::cout << " " << std::endl;
      //std::cout << "Setting boson with pdgId " << start_pdgId << " " << particle->pdgId() << std::endl;
      boson_beforeFSR = particle->p4(); 
      particle   = find_last_in_chain(particle);
      boson_afterFSR = particle->p4();
      //std::cout << "After last in chain " << start_pdgId << " " << particle->pdgId() << std::endl;
      //std::cout << "Boson has " << particle->nChildren() << std::endl;
      if (particle->pdgId() == 21 && !(particle->nChildren() == 2)){
	//std::cout << "Returning False " << std::endl;
	return false; ///-- We only want gluon ->bbar splitting --///
      }
      hasBoson = true;		
 
      ///-- Now look through the decays --///
      for (size_t k = 0; k < particle->nChildren(); k++) {
	//std::cout << "settiing boson child " << k << std::endl;
	const xAOD::TruthParticle* bosonChild = particle->child(k);
	//std::cout << "boson child is " << bosonChild << std::endl;
	if(!bosonChild) return false; ///-- let's not keep broken record events --///
	//std::cout << "boson isn't broken " << hasBosondecayProd1 << " " << hasBosondecayProd2 << std::endl;
	if(!hasBosondecayProd1){
	  ///std::cout << "setting boson child1 " << bosonChild->pdgId() << std::endl;
	  bosondecay1_beforeFSR = bosonChild->p4();
	  bosonChild = find_last_in_chain(bosonChild);
	  bosondecay1_afterFSR = bosonChild->p4();
	  bosondecay1_pdgId = particle->pdgId();
	  hasBosondecayProd1 = true;
	  //std::cout << "done setting boson child1" << std::endl;
	} else if (!hasBosondecayProd2){
	  //std::cout << "setting boson child2 " << bosonChild->pdgId() << std::endl;
	  bosondecay2_beforeFSR = bosonChild->p4();
	  bosonChild = find_last_in_chain(bosonChild);
	  bosondecay2_afterFSR = bosonChild->p4();
	  bosondecay2_pdgId = particle->pdgId();
	  hasBosondecayProd2 = true;
	  //std::cout << "done setting boson child2" << std::endl;
	} 
      }
      //std::cout << "Returning from top boson" << std::endl;
      return hasBoson && hasBosondecayProd1 && hasBosondecayProd2;
    }
    return false;
    
  }

  const xAOD::TruthParticle* CalcTopPartonHistory::find_last_in_chain(const xAOD::TruthParticle* particle) {
    
    //std::cout << "Number of children " << particle->nChildren() << std::endl;
    if( particle->nChildren() > 0 ){
      //std::cout << "Child 0 pdgId " << particle->child(0)->pdgId() << std::endl;
      while( particle->child(0) ){
	if(particle->child(0)->pdgId() == particle->pdgId()){
	  particle = particle->child(0);
	} else {
	  break;
	}
      }
    }
    return particle;
  }

  bool CalcTopPartonHistory::has_identical_parent(const xAOD::TruthParticle* particle) {

    for (size_t i = 0; i < particle->nParents(); i++) {
      const xAOD::TruthParticle* parent = particle->parent(i);
      if (parent && parent->pdgId() == particle->pdgId())
	return true;
      }
    return false;
  }

  bool CalcTopPartonHistory::Wlv( const xAOD::TruthParticleContainer* truthParticles,
				  TLorentzVector& W_p4,
				  TLorentzVector& Wdecay1_p4, int& Wdecay1_pdgId,
				  TLorentzVector& Wdecay2_p4, int& Wdecay2_pdgId){

    return false;
  }
      
  StatusCode CalcTopPartonHistory::execute()
  {
     // Get the Truth Particles
     const xAOD::TruthParticleContainer* truthParticles(nullptr);
     ATH_CHECK( evtStore()->retrieve( truthParticles , m_config->sgKeyMCParticle() ) );
    
     // Create the partonHistory xAOD object
     xAOD::PartonHistoryAuxContainer* partonAuxCont = new xAOD::PartonHistoryAuxContainer{};    
     xAOD::PartonHistoryContainer* partonCont = new xAOD::PartonHistoryContainer{};
     partonCont->setStore( partonAuxCont );
  
     xAOD::PartonHistory* partonHistory = new xAOD::PartonHistory{};
     partonCont->push_back( partonHistory );
          
     // Save to StoreGate / TStore
     std::string outputSGKey = m_config->sgKeyTopPartonHistory();
     std::string outputSGKeyAux = outputSGKey + "Aux.";
      
     xAOD::TReturnCode save = evtStore()->tds()->record( partonCont , outputSGKey );
     xAOD::TReturnCode saveAux = evtStore()->tds()->record( partonAuxCont , outputSGKeyAux );
     if( !save || !saveAux ){
       return StatusCode::FAILURE;
     }      
    
     return StatusCode::SUCCESS;
  } 
  
  void CalcTopPartonHistory::fillEtaBranch( xAOD::PartonHistory* partonHistory, std::string branchName, TLorentzVector &tlv){
     if (tlv.CosTheta()==1.) partonHistory->auxdecor< float >( branchName ) = 1000.;
     else if (tlv.CosTheta()==-1.) partonHistory->auxdecor< float >( branchName ) = 1000.;
     else partonHistory->auxdecor< float >( branchName ) = tlv.Eta();
     return;
     
     
  }
}
