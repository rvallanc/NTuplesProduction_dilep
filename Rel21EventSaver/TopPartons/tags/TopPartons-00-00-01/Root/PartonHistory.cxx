// $Id: PartonHistory.cxx 260035 2017-04-06 10:35:30Z jhowarth $
#include "TopPartons/PartonHistory.h"
#include "xAODCore/AddDVProxy.h"

using namespace std;

namespace xAOD{
  /// Aux Container   
  PartonHistoryAuxContainer::PartonHistoryAuxContainer() :
    AuxContainerBase() 
  {
  }
  
  /// Interface class
  PartonHistory::PartonHistory() :
    SG::AuxElement()
  {        
  }
  
  //Initialize variables for ttbar events
  void PartonHistory::IniVarTtbar(){
    //std::cout << "Initiating Ttbar Variables" << std::endl;
    std::vector< std::string > names;
    names.push_back("t");
    names.push_back("tbar");
    names.push_back("ttbar");
    names.push_back("b_from_t");
    names.push_back("bbar_from_tbar");
    names.push_back("W_from_t");
    names.push_back("W_from_t_decay1");
    names.push_back("W_from_t_decay2");
    names.push_back("W_from_tbar");
    names.push_back("W_from_tbar_decay1");
    names.push_back("W_from_tbar_decay2");

    for (int i = 0; i < names.size(); ++i){
      this->auxdecor< float >(names[i] + "_before_FSR_m")   = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_pt")  = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_eta") = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_phi") = -1000.;
      this->auxdecor< int >(  names[i] + "_before_FSR_pdgId") = -1;

      this->auxdecor< float >(names[i] + "_after_FSR_m")   = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_pt")  = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_eta") = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_phi") = -1000.;
      this->auxdecor< int >(  names[i] + "_after_FSR_pdgId") = -1;
    }    
    //std::cout << "Finished initiating Ttbar Variables" << std::endl;
  }
  
  void PartonHistory::IniVarHiggs(){
    //std::cout << "Initiating Variable Higgs" << std::endl;
    std::vector< std::string > names;
    names.push_back("H");
    names.push_back("H_decay1");
    names.push_back("H_decay2");

    for (int i = 0; i < names.size(); ++i){
      this->auxdecor< float >(names[i] + "_before_FSR_m")   = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_pt")  = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_eta") = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_phi") = -1000.;
      this->auxdecor< int >(  names[i] + "_before_FSR_pdgId") = -1;

      this->auxdecor< float >(names[i] + "_after_FSR_m")   = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_pt")  = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_eta") = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_phi") = -1000.;
      this->auxdecor< int >(  names[i] + "_after_FSR_pdgId") = -1;
    }    
    //std::cout << "Finished initiating Variable Higgs" << std::endl;
  }

  void PartonHistory::IniVarZBoson(){
    //std::cout << "Initiating Variable Z" << std::endl;
    std::vector< std::string > names;
    names.push_back("Z");
    names.push_back("Z_decay1");
    names.push_back("Z_decay2");

    for (int i = 0; i < names.size(); ++i){
      this->auxdecor< float >(names[i] + "_before_FSR_m")   = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_pt")  = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_eta") = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_phi") = -1000.;
      this->auxdecor< int >(  names[i] + "_before_FSR_pdgId") = -1;

      this->auxdecor< float >(names[i] + "_after_FSR_m")   = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_pt")  = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_eta") = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_phi") = -1000.;
      this->auxdecor< int >(  names[i] + "_after_FSR_pdgId") = -1;
    }    
    //std::cout << "Finished initiating Variable Z" << std::endl;
  }

  void PartonHistory::IniVarWBoson(){

    std::vector< std::string > names;
    names.push_back("W");
    names.push_back("W_decay1");
    names.push_back("W_decay2");

    for (int i = 0; i < names.size(); ++i){
      this->auxdecor< float >(names[i] + "_before_FSR_m")   = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_pt")  = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_eta") = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_phi") = -1000.;
      this->auxdecor< int >(  names[i] + "_before_FSR_pdgId") = -1;

      this->auxdecor< float >(names[i] + "_after_FSR_m")   = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_pt")  = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_eta") = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_phi") = -1000.;
      this->auxdecor< int >(  names[i] + "_after_FSR_pdgId") = -1;
    }    
  }

  void PartonHistory::IniVarGluon(){

    std::vector< std::string > names;
    names.push_back("gluon");
    names.push_back("gluon_decay1");
    names.push_back("gluon_decay2");

    for (int i = 0; i < names.size(); ++i){
      this->auxdecor< float >(names[i] + "_before_FSR_m")   = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_pt")  = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_eta") = -1000.;
      this->auxdecor< float >(names[i] + "_before_FSR_phi") = -1000.;
      this->auxdecor< int >(  names[i] + "_before_FSR_pdgId") = -1;

      this->auxdecor< float >(names[i] + "_after_FSR_m")   = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_pt")  = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_eta") = -1000.;
      this->auxdecor< float >(names[i] + "_after_FSR_phi") = -1000.;
      this->auxdecor< int >(  names[i] + "_after_FSR_pdgId") = -1;
    }    
  }

  //Initialize variables for tbbar events
  void PartonHistory::IniVarTbbar(){

   //ttbar variables  
   this->auxdecor< float >( "MC_tbbar_beforeFSR_m" ) = -1;
   this->auxdecor< float >( "MC_tbbar_beforeFSR_pt") = -1; 
   this->auxdecor< float >( "MC_tbbar_beforeFSR_eta" ) = -1000 ; 
   this->auxdecor< float >( "MC_tbbar_beforeFSR_phi" ) = -1000 ; 
   this->auxdecor< float >( "MC_tbbar_beforeFSR_pdgId" ) = -1 ; 

   this->auxdecor< float >( "MC_tbbar_afterFSR_m" ) = -1 ; 
   this->auxdecor< float >( "MC_tbbar_afterFSR_pt" ) = -1 ; 
   this->auxdecor< float >( "MC_tbbar_afterFSR_eta" ) = -1000 ; 
   this->auxdecor< float >( "MC_tbbar_afterFSR_phi" ) = -1000 ; 
   this->auxdecor< float >( "MC_tbbar_afterFSR_pdgId" ) = -1; 

   this->auxdecor< float >( "MC_t_beforeFSR_m" ) = -1 ; 
   this->auxdecor< float >( "MC_t_beforeFSR_pt" ) = -1 ; 
   this->auxdecor< float >( "MC_t_beforeFSR_eta" ) = -1000;
   this->auxdecor< float >( "MC_t_beforeFSR_phi" ) = -1000; 
   this->auxdecor< float >( "MC_t_beforeFSR_pdgId" ) = -1; 
   
   //tbbar variables  
   this->auxdecor< float >( "MC_b_beforeFSR_m" ) = -1 ; 
   this->auxdecor< float >( "MC_b_beforeFSR_pt" ) = -1 ;
   this->auxdecor< float >( "MC_b_beforeFSR_eta" ) = -1000 ;
   this->auxdecor< float >( "MC_b_beforeFSR_phi" ) = -1000 ; 
   this->auxdecor< float >( "MC_b_beforeFSR_pdgId" ) = -1 ; 
   
   this->auxdecor< float >( "MC_b_afterFSR_m" ) = -1 ; 
   this->auxdecor< float >( "MC_b_afterFSR_pt" ) = -1 ;
   this->auxdecor< float >( "MC_b_afterFSR_eta" ) = -1000 ;
   this->auxdecor< float >( "MC_b_afterFSR_phi" ) = -1000 ; 
   this->auxdecor< float >( "MC_b_afterFSR_pdgId" ) = -1 ; 

   this->auxdecor< float >( "MC_W_from_t_m" ) = -1 ; 
   this->auxdecor< float >( "MC_W_from_t_pt" ) = -1 ; 
   this->auxdecor< float >( "MC_W_from_t_eta" ) = -1000 ; 
   this->auxdecor< float >( "MC_W_from_t_phi" ) = -1000 ; 
   this->auxdecor< float >( "MC_W_from_t_pdgId" ) = -1 ; 

   this->auxdecor< float >( "MC_b_from_t_m" ) = -1 ; 
   this->auxdecor< float >( "MC_b_from_t_pt" ) = -1 ; 
   this->auxdecor< float >( "MC_b_from_t_eta" ) = -1000 ; 
   this->auxdecor< float >( "MC_b_from_t_phi" ) = -1000 ; 
   this->auxdecor< float >( "MC_b_from_t_pdgId" ) = -1; 

   this->auxdecor< float >( "MC_Wdecay1_from_t_m" ) = -1 ;
   this->auxdecor< float >( "MC_Wdecay1_from_t_pt" ) = -1 ;
   this->auxdecor< float >( "MC_Wdecay1_from_t_eta" ) = -1000 ; 
   this->auxdecor< float >( "MC_Wdecay1_from_t_phi" ) = -1000 ; 
   this->auxdecor< int >(   "MC_Wdecay1_from_t_pdgId") = 0 ; 

   this->auxdecor< float >( "MC_Wdecay2_from_t_m" ) = -1 ; 
   this->auxdecor< float >( "MC_Wdecay2_from_t_pt" ) = -1 ; 
   this->auxdecor< float >( "MC_Wdecay2_from_t_eta" ) = -1000 ; 
   this->auxdecor< float >( "MC_Wdecay2_from_t_phi" ) = -1000 ; 
   this->auxdecor< int >(   "MC_Wdecay2_from_t_pdgId") = 0 ; 

  }
  
    //Initialize variables for Wlv events
  void PartonHistory::IniVarWlv(){
	
   // W
   this->auxdecor< float >( "MC_W_m" ) = -1 ; 
   this->auxdecor< float >( "MC_W_pt" ) = -1 ; 
   this->auxdecor< float >( "MC_W_eta" ) = -1000 ; 
   this->auxdecor< float >( "MC_W_phi" ) = -1000 ; 

   this->auxdecor< float >( "MC_l_m" ) = -1 ;
   this->auxdecor< float >( "MC_l_pt" ) = -1 ;
   this->auxdecor< float >( "MC_l_eta" ) = -1000 ; 
   this->auxdecor< float >( "MC_l_phi" ) = -1000 ; 
   this->auxdecor< int >( "MC_l_pdgId") = 0 ; 

   this->auxdecor< float >( "MC_v_m" ) = -1 ; 
   this->auxdecor< float >( "MC_v_pt" ) = -1 ; 
   this->auxdecor< float >( "MC_v_eta" ) = -1000 ; 
   this->auxdecor< float >( "MC_v_phi" ) = -1000 ; 
   this->auxdecor< int >( "MC_v_pdgId") = 0 ; 

  }


}
ADD_NS_DV_PROXY( xAOD , PartonHistoryContainer );
